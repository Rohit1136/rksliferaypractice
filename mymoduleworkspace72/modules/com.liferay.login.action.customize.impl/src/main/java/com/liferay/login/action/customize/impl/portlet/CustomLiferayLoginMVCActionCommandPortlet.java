package com.liferay.login.action.customize.impl.portlet;

import com.liferay.login.action.customize.impl.constants.CustomLiferayLoginMVCActionCommandPortletKeys;
import com.liferay.portal.kernel.model.CompanyConstants;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author Rohit Kumar Singh
 */
@Component(
		property = {
			"javax.portlet.name=" + CustomLiferayLoginMVCActionCommandPortletKeys.FAST_LOGIN,
			"javax.portlet.name=" + CustomLiferayLoginMVCActionCommandPortletKeys.LOGIN,
			"mvc.command.name=/login/login",
			"service.ranking:Integer=100"
		},
		service = MVCActionCommand.class
	)
public class CustomLiferayLoginMVCActionCommandPortlet extends BaseMVCActionCommand {

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		System.out.println("CustomLiferayLoginMVCActionCommandPortlet doProcessAction starts!!");
		String login=ParamUtil.getString(actionRequest, "login");
		String password=ParamUtil.getString(actionRequest, "password");
		boolean rememberMe=ParamUtil.getBoolean(actionRequest, "rememberMe");
		String authType=CompanyConstants.AUTH_TYPE_EA;
		System.out.println("login: "+login+", Password: "+password+", RemeberMe: "+rememberMe+", AuthType: "+authType);
		if(true) {
			mvcActionCommand.processAction(actionRequest,actionResponse);
		}
	}
	
	@Reference(target = "(&(mvc.command.name=/login/login)"
			+"(javax.portlet.name=com_liferay_login_web_portlet_LoginPortlet)"
			+"(javax.portlet.name=com_liferay_login_web_portlet_FastLoginPortlet)"
			+"(component.name=com.liferay.login.web.internal.portlet.action.LoginMVCActionCommand))")
	protected MVCActionCommand mvcActionCommand;
}