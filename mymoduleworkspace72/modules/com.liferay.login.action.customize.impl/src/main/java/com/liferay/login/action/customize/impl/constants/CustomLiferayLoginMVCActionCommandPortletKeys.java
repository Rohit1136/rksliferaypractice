package com.liferay.login.action.customize.impl.constants;

/**
 * @author Rohit Kumar Singh
 */
public class CustomLiferayLoginMVCActionCommandPortletKeys {

	public static final String CUSTOMLIFERAYLOGINMVCACTIONCOMMAND =
		"com_liferay_login_action_customize_impl_CustomLiferayLoginMVCActionCommandPortlet";
	
	public static final String FAST_LOGIN =
			"com_liferay_login_web_portlet_FastLoginPortlet";

	public static final String LOGIN =
			"com_liferay_login_web_portlet_LoginPortlet";
}