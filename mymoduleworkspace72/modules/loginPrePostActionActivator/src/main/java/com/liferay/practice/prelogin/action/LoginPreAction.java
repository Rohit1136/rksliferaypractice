package com.liferay.practice.prelogin.action;

import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.LifecycleAction;
import com.liferay.portal.kernel.events.LifecycleEvent;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.PortalUtil;

import java.net.HttpRetryException;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;

@Component(
	immediate = true,
	property = {
		"key=login.events.pre"
	},
	service = LifecycleAction.class
)
public class LoginPreAction implements LifecycleAction {

	@Override
	public void processLifecycleEvent(LifecycleEvent lifecycleEvent)
		throws ActionException {

		System.out.println("login.event.pre=" + lifecycleEvent);
		
		HttpServletRequest request=lifecycleEvent.getRequest();
		long userId=0;
		try {
			userId=PortalUtil.getUser(request).getUserId();
		} catch(PortalException e) {
			System.out.println("Error is: "+e);
		}
		System.out.println("login.event.pre, userId : "+userId);
	}

}