package com.liferay.prepost.action.activator.constants;

/**
 * @author Rohit Kumar Singh
 */
public class LoginPrePostActionActivatorPortletKeys {

	public static final String LOGINPREPOSTACTIONACTIVATOR =
		"com_liferay_prepost_action_activator_LoginPrePostActionActivatorPortlet";

}