/**
 * Copyright 2000-present Liferay, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.liferay.practice.postlogin.action;

import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.LifecycleAction;
import com.liferay.portal.kernel.events.LifecycleEvent;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.PortalUtil;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;

@Component(
	immediate = true,
	property = {
		"key=login.events.post"
	},
	service = LifecycleAction.class
)
public class LoginPostAction implements LifecycleAction {

	@Override
	public void processLifecycleEvent(LifecycleEvent lifecycleEvent)
		throws ActionException {

		System.out.println("login.event.Post=" + lifecycleEvent);

		HttpServletRequest request=lifecycleEvent.getRequest();
		long userId=0;
		String name="";
		boolean loginFlag=false;
		try {
			userId=PortalUtil.getUser(request).getUserId();
			name=PortalUtil.getUser(request).getFullName();
			loginFlag=PortalUtil.getUser(request).isEmailAddressVerificationComplete();
		} catch(PortalException e) {
			System.out.println("Error is: "+e);
		}
		System.out.println("login.event.post, userId : "+userId+", Fullname: "+name+", loginFlag: "+loginFlag);
	}

}