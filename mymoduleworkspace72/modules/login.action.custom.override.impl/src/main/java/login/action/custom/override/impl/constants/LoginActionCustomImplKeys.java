package login.action.custom.override.impl.constants;

/**
 * @author Rohit Kumar Singh
 */
public class LoginActionCustomImplKeys {

	public static final String LOGINACTIONCUSTOMIMPL =
		"login_action_custom_override_impl_LoginActionCustomImplPortlet";
		
	public static final String FAST_LOGIN =
			"com_liferay_login_web_portlet_FastLoginPortlet";

	public static final String LOGIN =
			"com_liferay_login_web_portlet_LoginPortlet";
}