package login.action.custom.override.impl.portlet;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.CompanyConstants;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.security.auth.session.AuthenticatedSessionManagerUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;

import login.action.custom.override.impl.constants.LoginActionCustomImplKeys;

/**
 * @author Rohit Kumar Singh
 */
@Component(
		property = {
			"javax.portlet.name=" + LoginActionCustomImplKeys.FAST_LOGIN,
			"javax.portlet.name=" + LoginActionCustomImplKeys.LOGIN,
			"mvc.command.name=/login/login",
			"service.ranking:Integer=1000"
		},
		service = MVCActionCommand.class
)
public class LoginActionCustomImpl extends BaseMVCActionCommand {
	
	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		_logger.info("LoginActionCustomImpl doProcessAction starts!!");
		ThemeDisplay themeDisplay=(ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		HttpServletRequest httpServletRequest=PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(actionRequest));
		HttpServletResponse httpServletResponse=PortalUtil.getHttpServletResponse(actionResponse);
		String login=ParamUtil.getString(actionRequest, "login");
		//String password=actionRequest.getParameter("password");
		String password=ParamUtil.getString(actionRequest, "password");
		boolean rememberMe=ParamUtil.getBoolean(actionRequest, "rememberMe");
		String authType=CompanyConstants.AUTH_TYPE_EA;
		
		_logger.info("login: "+login+", Password: "+password+", RemeberMe: "+rememberMe+", AuthType: "+authType);
		AuthenticatedSessionManagerUtil.login(httpServletRequest, httpServletResponse, login, password, rememberMe, authType);
		actionResponse.sendRedirect(themeDisplay.getPathMain());
	}
	private static Log _logger=LogFactoryUtil.getLog(LoginActionCustomImpl.class);
}