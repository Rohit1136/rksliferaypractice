package com.liferay.practice.adduseradmin.custom.constants;

/**
 * @author Rohit Kumar Singh
 */
public class AddUsersAdminCustomKeys {

	public static final String ADDUSERSADMINCUSTOM =
		"com_liferay_practice_adduseradmin_custom_AddUsersAdminCustomPortlet";
	public static final String 	MY_ORGANIZATIONS="com_liferay_users_admin_web_portlet_MyOrganizationsPortlet";
	public static final String  USERS_ADMIN="com_liferay_users_admin_web_portlet_UsersAdminPortlet";
}