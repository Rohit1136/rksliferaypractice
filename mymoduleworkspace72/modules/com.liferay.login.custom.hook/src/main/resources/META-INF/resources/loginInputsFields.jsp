<link href="${pageContext.request.contextPath}/css/icon.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/materialize.min.css" rel="stylesheet">
<script src="${pageContext.request.contextPath}/js/materialize.min.js" type="text/javascript"></script>
 

<div class=""> 
	<div id="user-nam" class="input-group inp-login inpuser" style="margin-top: -17px; border-color: rgb(0, 119, 190);"> 
		<span id="inputGroupAddon01" class="input-group-addon"> 
			<i class="material-icons inpuser" style="border-color: rgb(0, 119, 190);">account_circle</i> 
		</span> 
		<div class="form-group input-text-wrapper has-error"> 
			<label class="control-label active" for="_com_liferay_login_web_portlet_LoginPortlet_login"> User Name </label> 
			<input class="field clearable form-control error-field" id="_com_liferay_login_web_portlet_LoginPortlet_login" name="_com_liferay_login_web_portlet_LoginPortlet_login" placeholder="User Name" type="text" value="" aria-required="true" style="color: rgb(0, 119, 190);" aria-describedby="_com_liferay_login_web_portlet_LoginPortlet_loginHelper" aria-invalid="true">
			<div class="form-validator-stack help-block" id="_com_liferay_login_web_portlet_LoginPortlet_loginHelper">
				<div role="alert" class="required">This field is required.
				</div>
			</div> 
		</div> 
		<label class="lab-user-name" style="display: none;">User Name</label> 
	</div> 

	<div id="user-pwd" class="input-group inp-login inppwd " style="margin-top:32px;"> 
		<span id="inputGroupAddon01" class="input-group-addon"> 
			<i class="material-icons">vpn_key</i> 
			<i class="material-icons eyefirst" style="right:10px;cursor: pointer; position:absolute;z-index:3;">visibility</i> 
			<i class="material-icons eyefirstoff" style=" right:10px;cursor: pointer;display:none;position:absolute;z-index:3;">visibility_off</i> 
		</span> 

		<div class="form-group input-text-wrapper"> 
			<label class="control-label active" for="_com_liferay_login_web_portlet_LoginPortlet_user-pwds"> Password </label> 
			<input class="field form-control" id="_com_liferay_login_web_portlet_LoginPortlet_user-pwds" name="_com_liferay_login_web_portlet_LoginPortlet_password" placeholder="Password" type="password" value="" style="z-index: 2;" aria-required="true"> 
		</div> 
		<label class="lab-user-pwd" style="display:none;">Password</label> 
	</div> 
	<span id="_com_liferay_login_web_portlet_LoginPortlet_passwordCapsLockSpan" style="display: none;">Caps Lock is on.</span>
</div>