<%@ include file="/init.jsp" %>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>

.modal-header {
  align-items: center;
  display: none;
  
 }


#signinmodal{
width: 450px !important;
left: 530px !important;
top: -29px;
z-index: 999;
position: absolute;
}


.foot-log {
  background-attachment: scroll;
  background-clip: border-box;
  background-color: #fff;
  background-image: none;
  background-origin: padding-box;
  background-position-x: 0;
  background-position-y: 0;
  background-repeat: repeat;
  background-size: auto auto;
  border-bottom-left-radius: 6px;
  border-bottom-right-radius: 6px;
  color: #888;
  margin-bottom: -21px;
  margin-left: -11px;
  margin-right: -11px;
  margin-top: 2px;
  padding: 20px 6px;
}

.defualtBtn {
  width: 100%;
  background: #cfcfcf;
  background-color: rgb(207, 207, 207);
  color: #fff;
}

.red {
  background-color: #F44336 !important;
}

body {
  font-size: 14px;
  line-height: 1.42857;
}

.styleSignInBtn{
  color:#fff;
  display: block;
  background-color:#f44336;
  margin-top: 18px;
  width: 100%;
  border: 1px solid #f44336;
  height: 36px;
  padding: 9px !important;
  text-decoration: none;
  text-transform: uppercase;
  box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);
}
/* .button-holder {
    margin: 10px 0 !important;
} */

.emailInputFiled {
	text-decoration: none;
    /* border-color: rgb(0, 119, 190); */
	height:53px !important;
	padding:5px;
	width:99%;
	background-color:#fff;
	margin-top: 13px;
	border-radius: 4px;
	border-color:#000000;
}

.passwordInputFields {
    text-decoration: none;
	height:53px !important;
	padding:5px;
	width:99%;
	background-color:#fff;
	margin-top: 13px;
	border-radius: 4px;
    border-color:#000000;
    
}

.modal-body{
max-height: 450px; 
height: 440px;
bottom: 12px;
width: 464px;
top: 0px;
padding-bottom: 74px;
}

</style>

<c:choose>
	<c:when test="<%= themeDisplay.isSignedIn() %>">

		<%
		String signedInAs = HtmlUtil.escape(user.getFullName());

		if (themeDisplay.isShowMyAccountIcon() && (themeDisplay.getURLMyAccount() != null)) {
			String myAccountURL = String.valueOf(themeDisplay.getURLMyAccount());

			signedInAs = "<a class=\"signed-in\" href=\"" + HtmlUtil.escape(myAccountURL) + "\">" + signedInAs + "</a>";
		}
		%>

		<liferay-ui:message arguments="<%= signedInAs %>" key="you-are-signed-in-as-x" translateArguments="<%= false %>" />
</c:when>

<c:otherwise>

		 <%
		String formName = "loginForm";

		if (windowState.equals(LiferayWindowState.EXCLUSIVE)) {
			formName += "Modal";
		}

		String redirect = ParamUtil.getString(request, "redirect");

		String login = (String)SessionErrors.get(renderRequest, "login");

		if (Validator.isNull(login)) {
			login = LoginUtil.getLogin(request, "login", company);
		}

		String password = StringPool.BLANK;
		boolean rememberMe = ParamUtil.getBoolean(request, "rememberMe");

		if (Validator.isNull(authType)) {
			authType = company.getAuthType();
		}
		%>

		<div class="login-container" style="overflow-y: none !important;overflow-x: none !important">
			<portlet:actionURL name="/login/login" secure="<%= PropsValues.COMPANY_SECURITY_AUTH_REQUIRES_HTTPS || request.isSecure() %>" var="loginURL">
				<portlet:param name="mvcRenderCommandName" value="/login/login" />
			</portlet:actionURL>
			
			<c:set var="context" value="${pageContext.request.contextPath}/images/britamLogo.jpg"/>
			<center><img alt="image" src="${context }" border="0"/></center>

			<aui:form action="<%= loginURL %>" autocomplete='<%= PropsValues.COMPANY_SECURITY_LOGIN_FORM_AUTOCOMPLETE ? "on" : "off" %>' cssClass="sign-in-form" method="post" name="<%= formName %>" onSubmit="event.preventDefault();" validateOnBlur="<%= false %>">
				<aui:input name="saveLastPath" type="hidden" value="<%= false %>" />
				<aui:input name="redirect" type="hidden" value="<%= redirect %>" />
				<aui:input name="doActionAfterLogin" type="hidden" value="<%= portletName.equals(PortletKeys.FAST_LOGIN) ? true : false %>" />

				<div class="inline-alert-container lfr-alert-container"></div>

				<liferay-util:dynamic-include key="com.liferay.login.web#/login.jsp#alertPre" />

				<c:choose>
					<c:when test='<%= SessionMessages.contains(request, "forgotPasswordSent") %>'>
						<div class="alert alert-success">
							<liferay-ui:message key="your-request-completed-successfully" />
						</div>
					</c:when>
					<c:when test='<%= SessionMessages.contains(request, "userAdded") %>'>

						<%
						String userEmailAddress = (String)SessionMessages.get(request, "userAdded");
						%>

						<div class="alert alert-success">
							<liferay-ui:message key="thank-you-for-creating-an-account" />

							<c:if test="<%= company.isStrangersVerify() %>">
								<liferay-ui:message arguments="<%= HtmlUtil.escape(userEmailAddress) %>" key="your-email-verification-code-was-sent-to-x" translateArguments="<%= false %>" />
							</c:if>

							<c:if test="<%= PrefsPropsUtil.getBoolean(company.getCompanyId(), PropsKeys.ADMIN_EMAIL_USER_ADDED_ENABLED) %>">
								<c:choose>
									<c:when test="<%= PropsValues.LOGIN_CREATE_ACCOUNT_ALLOW_CUSTOM_PASSWORD %>">
										<liferay-ui:message key="use-your-password-to-login" />
									</c:when>
									<c:otherwise>
										<liferay-ui:message arguments="<%= HtmlUtil.escape(userEmailAddress) %>" key="you-can-set-your-password-following-instructions-sent-to-x" translateArguments="<%= false %>" />
									</c:otherwise>
								</c:choose>
							</c:if>
						</div>
					</c:when>
					<c:when test='<%= SessionMessages.contains(request, "userPending") %>'>

						<%
						String userEmailAddress = (String)SessionMessages.get(request, "userPending");
						%>

						<div class="alert alert-success">
							<liferay-ui:message arguments="<%= HtmlUtil.escape(userEmailAddress) %>" key="thank-you-for-creating-an-account.-you-will-be-notified-via-email-at-x-when-your-account-has-been-approved" translateArguments="<%= false %>" />
						</div>
					</c:when>
				</c:choose>

				<liferay-ui:error exception="<%= AuthException.class %>" message="authentication-failed" />
				<liferay-ui:error exception="<%= CompanyMaxUsersException.class %>" message="unable-to-log-in-because-the-maximum-number-of-users-has-been-reached" />
				<liferay-ui:error exception="<%= CookieNotSupportedException.class %>" message="authentication-failed-please-enable-browser-cookies" />
				<liferay-ui:error exception="<%= NoSuchUserException.class %>" message="authentication-failed" />
				<liferay-ui:error exception="<%= PasswordExpiredException.class %>" message="your-password-has-expired" />
				<liferay-ui:error exception="<%= UserEmailAddressException.MustNotBeNull.class %>" message="please-enter-an-email-address" />
				<liferay-ui:error exception="<%= UserLockoutException.LDAPLockout.class %>" message="this-account-is-locked" />

				<liferay-ui:error exception="<%= UserLockoutException.PasswordPolicyLockout.class %>">

					<%
					UserLockoutException.PasswordPolicyLockout ule = (UserLockoutException.PasswordPolicyLockout)errorException;
					%>

					<c:choose>
						<c:when test="<%= ule.passwordPolicy.isRequireUnlock() %>">
							<liferay-ui:message key="this-account-is-locked" />
						</c:when>
						<c:otherwise>

							<%
							Format dateFormat = FastDateFormatFactoryUtil.getDateTime(FastDateFormatConstants.SHORT, FastDateFormatConstants.LONG, locale, TimeZone.getTimeZone(ule.user.getTimeZoneId()));
							%>

							<liferay-ui:message arguments="<%= dateFormat.format(ule.user.getUnlockDate()) %>" key="this-account-is-locked-until-x" translateArguments="<%= false %>" />
						</c:otherwise>
					</c:choose>
				</liferay-ui:error>

				<liferay-ui:error exception="<%= UserPasswordException.class %>" message="authentication-failed" />
				<liferay-ui:error exception="<%= UserScreenNameException.MustNotBeNull.class %>" message="the-screen-name-cannot-be-blank" />

				<liferay-util:dynamic-include key="com.liferay.login.web#/login.jsp#alertPost" />

				<aui:fieldset>

					<%
					String loginLabel = null;

					if (authType.equals(CompanyConstants.AUTH_TYPE_EA)) {
						loginLabel = "email-address";
					}
					else if (authType.equals(CompanyConstants.AUTH_TYPE_SN)) {
						loginLabel = "screen-name";
					}
					else if (authType.equals(CompanyConstants.AUTH_TYPE_ID)) {
						loginLabel = "id";
					}
					%>
					<%-- <%@ include file="/loginInputsFields.jsp"%> --%>
					
						   <%--  <aui:input  cssClass="emailInputFiled" label="" name="login" type="text" value="" placeholder="User Name">
							
								<aui:validator name="required" />
		
								<c:if test="<%= authType.equals(CompanyConstants.AUTH_TYPE_EA) %>">
									<aui:validator name="email" />
								</c:if>
							</aui:input> --%>
									
				   <!-- <div class="input-icons" style="margin-top: 30px;">
				   		<i class="fa fa-user icon"></i> -->
						<input class="emailInputFiled" name="<portlet:namespace/>login" type="email" value="" placeholder="User Name" required="required"/> 
					<!-- </div> -->
					<%-- <aui:input id="password" cssClass="passwordInputFields" name="password" label="" type="password" value="<%= password %>" placeholder="Password">
						<i class="bi bi-eye-slash" id="togglePassword" style="float:right;"></i>
						<aui:validator name="required" />
					</aui:input> --%>
					
						<input id="id_password" class="passwordInputFields" name="<portlet:namespace/>password" type="password" value="<%=password %>" placeholder="Password" required="required">
 						<i class="far fa-eye" id="togglePassword" style="margin-left: -30px; cursor: pointer;"></i>
						<span id="<portlet:namespace />passwordCapsLockSpan" style="display: none;"><liferay-ui:message key="caps-lock-is-on" /></span>

					<%-- <c:if test="<%= company.isAutoLogin() && !PropsValues.SESSION_DISABLED %>">
						<aui:input checked="<%= rememberMe %>" name="rememberMe" type="checkbox" />
					</c:if> --%>
				</aui:fieldset>

				<!-- <aui:button-row>
					<aui:button cssClass="styleSignInBtn" type="submit" value="sign-in" />
				</aui:button-row> -->
				<button class="styleSignInBtn" type="submit">sign in</button>
			</aui:form>

			<%@ include file="/navigation.jspf" %>
			
			<div class="text-center foot-log"> <p>Britam � 2022, All rights reserved</p> </div>
		</div>
		<aui:script sandbox="<%= true %>">
			var form = document.getElementById('<portlet:namespace /><%= formName %>');

			if (form) {
				form.addEventListener('submit', function(event) {
					<c:if test="<%= Validator.isNotNull(redirect) %>">
						var redirect = form.querySelector('#<portlet:namespace />redirect');

						if (redirect) {
							var redirectVal = redirect.getAttribute('value');

							redirect.setAttribute('value', redirectVal + window.location.hash);
						}
					</c:if>

					submitForm(form);
				});

				var password = form.querySelector('#<portlet:namespace />password');

				if (password) {
					password.addEventListener('keypress', function(event) {
						Liferay.Util.showCapsLock(
							event,
							'<portlet:namespace />passwordCapsLockSpan'
						);
					});
				}
			}
			
		
		</aui:script>
		<script type="text/javascript">
		  const togglePassword = document.querySelector('#togglePassword');
		  const password = document.querySelector('#id_password');
		 
		  togglePassword.addEventListener('click', function (e) {
	      // toggle the type attribute
	      const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
	      password.setAttribute('type', type);
	      // toggle the eye slash icon
	      this.classList.toggle('fa-eye-slash');
		});
        
		
	        
	     // prevent form submit
	        /* const form = document.querySelector("form");
	        form.addEventListener('submit', function (e) {
	            e.preventDefault();
	        }); */
		</script>
		
       

	</c:otherwise>
</c:choose>
