/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.service.mymodules.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.service.mymodules.model.usersExt;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osgi.annotation.versioning.ProviderType;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The persistence utility for the users ext service. This utility wraps <code>com.liferay.service.mymodules.service.persistence.impl.usersExtPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author rohit
 * @see usersExtPersistence
 * @generated
 */
@ProviderType
public class usersExtUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(usersExt usersExt) {
		getPersistence().clearCache(usersExt);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, usersExt> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<usersExt> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<usersExt> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<usersExt> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<usersExt> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static usersExt update(usersExt usersExt) {
		return getPersistence().update(usersExt);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static usersExt update(
		usersExt usersExt, ServiceContext serviceContext) {

		return getPersistence().update(usersExt, serviceContext);
	}

	/**
	 * Returns all the users exts where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching users exts
	 */
	public static List<usersExt> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the users exts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @return the range of matching users exts
	 */
	public static List<usersExt> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the users exts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByUuid(String, int, int, OrderByComparator)}
	 * @param uuid the uuid
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching users exts
	 */
	@Deprecated
	public static List<usersExt> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<usersExt> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns an ordered range of all the users exts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching users exts
	 */
	public static List<usersExt> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<usersExt> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns the first users ext in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public static usersExt findByUuid_First(
			String uuid, OrderByComparator<usersExt> orderByComparator)
		throws com.liferay.service.mymodules.exception.NoSuchusersExtException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first users ext in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public static usersExt fetchByUuid_First(
		String uuid, OrderByComparator<usersExt> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last users ext in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public static usersExt findByUuid_Last(
			String uuid, OrderByComparator<usersExt> orderByComparator)
		throws com.liferay.service.mymodules.exception.NoSuchusersExtException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last users ext in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public static usersExt fetchByUuid_Last(
		String uuid, OrderByComparator<usersExt> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the users exts before and after the current users ext in the ordered set where uuid = &#63;.
	 *
	 * @param usersExtId the primary key of the current users ext
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next users ext
	 * @throws NoSuchusersExtException if a users ext with the primary key could not be found
	 */
	public static usersExt[] findByUuid_PrevAndNext(
			long usersExtId, String uuid,
			OrderByComparator<usersExt> orderByComparator)
		throws com.liferay.service.mymodules.exception.NoSuchusersExtException {

		return getPersistence().findByUuid_PrevAndNext(
			usersExtId, uuid, orderByComparator);
	}

	/**
	 * Removes all the users exts where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of users exts where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching users exts
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the users ext where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchusersExtException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public static usersExt findByUUID_G(String uuid, long groupId)
		throws com.liferay.service.mymodules.exception.NoSuchusersExtException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the users ext where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #fetchByUUID_G(String,long)}
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	@Deprecated
	public static usersExt fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Returns the users ext where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public static usersExt fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Removes the users ext where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the users ext that was removed
	 */
	public static usersExt removeByUUID_G(String uuid, long groupId)
		throws com.liferay.service.mymodules.exception.NoSuchusersExtException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of users exts where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching users exts
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the users exts where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching users exts
	 */
	public static List<usersExt> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the users exts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @return the range of matching users exts
	 */
	public static List<usersExt> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the users exts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByUuid_C(String,long, int, int, OrderByComparator)}
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching users exts
	 */
	@Deprecated
	public static List<usersExt> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<usersExt> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns an ordered range of all the users exts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching users exts
	 */
	public static List<usersExt> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<usersExt> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the first users ext in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public static usersExt findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<usersExt> orderByComparator)
		throws com.liferay.service.mymodules.exception.NoSuchusersExtException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first users ext in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public static usersExt fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<usersExt> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last users ext in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public static usersExt findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<usersExt> orderByComparator)
		throws com.liferay.service.mymodules.exception.NoSuchusersExtException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last users ext in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public static usersExt fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<usersExt> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the users exts before and after the current users ext in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param usersExtId the primary key of the current users ext
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next users ext
	 * @throws NoSuchusersExtException if a users ext with the primary key could not be found
	 */
	public static usersExt[] findByUuid_C_PrevAndNext(
			long usersExtId, String uuid, long companyId,
			OrderByComparator<usersExt> orderByComparator)
		throws com.liferay.service.mymodules.exception.NoSuchusersExtException {

		return getPersistence().findByUuid_C_PrevAndNext(
			usersExtId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the users exts where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of users exts where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching users exts
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the users exts where usersExtId = &#63;.
	 *
	 * @param usersExtId the users ext ID
	 * @return the matching users exts
	 */
	public static List<usersExt> findByUserExtId(long usersExtId) {
		return getPersistence().findByUserExtId(usersExtId);
	}

	/**
	 * Returns a range of all the users exts where usersExtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param usersExtId the users ext ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @return the range of matching users exts
	 */
	public static List<usersExt> findByUserExtId(
		long usersExtId, int start, int end) {

		return getPersistence().findByUserExtId(usersExtId, start, end);
	}

	/**
	 * Returns an ordered range of all the users exts where usersExtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByUserExtId(long, int, int, OrderByComparator)}
	 * @param usersExtId the users ext ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching users exts
	 */
	@Deprecated
	public static List<usersExt> findByUserExtId(
		long usersExtId, int start, int end,
		OrderByComparator<usersExt> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUserExtId(
			usersExtId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns an ordered range of all the users exts where usersExtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param usersExtId the users ext ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching users exts
	 */
	public static List<usersExt> findByUserExtId(
		long usersExtId, int start, int end,
		OrderByComparator<usersExt> orderByComparator) {

		return getPersistence().findByUserExtId(
			usersExtId, start, end, orderByComparator);
	}

	/**
	 * Returns the first users ext in the ordered set where usersExtId = &#63;.
	 *
	 * @param usersExtId the users ext ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public static usersExt findByUserExtId_First(
			long usersExtId, OrderByComparator<usersExt> orderByComparator)
		throws com.liferay.service.mymodules.exception.NoSuchusersExtException {

		return getPersistence().findByUserExtId_First(
			usersExtId, orderByComparator);
	}

	/**
	 * Returns the first users ext in the ordered set where usersExtId = &#63;.
	 *
	 * @param usersExtId the users ext ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public static usersExt fetchByUserExtId_First(
		long usersExtId, OrderByComparator<usersExt> orderByComparator) {

		return getPersistence().fetchByUserExtId_First(
			usersExtId, orderByComparator);
	}

	/**
	 * Returns the last users ext in the ordered set where usersExtId = &#63;.
	 *
	 * @param usersExtId the users ext ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public static usersExt findByUserExtId_Last(
			long usersExtId, OrderByComparator<usersExt> orderByComparator)
		throws com.liferay.service.mymodules.exception.NoSuchusersExtException {

		return getPersistence().findByUserExtId_Last(
			usersExtId, orderByComparator);
	}

	/**
	 * Returns the last users ext in the ordered set where usersExtId = &#63;.
	 *
	 * @param usersExtId the users ext ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public static usersExt fetchByUserExtId_Last(
		long usersExtId, OrderByComparator<usersExt> orderByComparator) {

		return getPersistence().fetchByUserExtId_Last(
			usersExtId, orderByComparator);
	}

	/**
	 * Removes all the users exts where usersExtId = &#63; from the database.
	 *
	 * @param usersExtId the users ext ID
	 */
	public static void removeByUserExtId(long usersExtId) {
		getPersistence().removeByUserExtId(usersExtId);
	}

	/**
	 * Returns the number of users exts where usersExtId = &#63;.
	 *
	 * @param usersExtId the users ext ID
	 * @return the number of matching users exts
	 */
	public static int countByUserExtId(long usersExtId) {
		return getPersistence().countByUserExtId(usersExtId);
	}

	/**
	 * Returns all the users exts where officeId = &#63;.
	 *
	 * @param officeId the office ID
	 * @return the matching users exts
	 */
	public static List<usersExt> findByOfficeId(String officeId) {
		return getPersistence().findByOfficeId(officeId);
	}

	/**
	 * Returns a range of all the users exts where officeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param officeId the office ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @return the range of matching users exts
	 */
	public static List<usersExt> findByOfficeId(
		String officeId, int start, int end) {

		return getPersistence().findByOfficeId(officeId, start, end);
	}

	/**
	 * Returns an ordered range of all the users exts where officeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByOfficeId(String, int, int, OrderByComparator)}
	 * @param officeId the office ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching users exts
	 */
	@Deprecated
	public static List<usersExt> findByOfficeId(
		String officeId, int start, int end,
		OrderByComparator<usersExt> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByOfficeId(
			officeId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns an ordered range of all the users exts where officeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param officeId the office ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching users exts
	 */
	public static List<usersExt> findByOfficeId(
		String officeId, int start, int end,
		OrderByComparator<usersExt> orderByComparator) {

		return getPersistence().findByOfficeId(
			officeId, start, end, orderByComparator);
	}

	/**
	 * Returns the first users ext in the ordered set where officeId = &#63;.
	 *
	 * @param officeId the office ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public static usersExt findByOfficeId_First(
			String officeId, OrderByComparator<usersExt> orderByComparator)
		throws com.liferay.service.mymodules.exception.NoSuchusersExtException {

		return getPersistence().findByOfficeId_First(
			officeId, orderByComparator);
	}

	/**
	 * Returns the first users ext in the ordered set where officeId = &#63;.
	 *
	 * @param officeId the office ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public static usersExt fetchByOfficeId_First(
		String officeId, OrderByComparator<usersExt> orderByComparator) {

		return getPersistence().fetchByOfficeId_First(
			officeId, orderByComparator);
	}

	/**
	 * Returns the last users ext in the ordered set where officeId = &#63;.
	 *
	 * @param officeId the office ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public static usersExt findByOfficeId_Last(
			String officeId, OrderByComparator<usersExt> orderByComparator)
		throws com.liferay.service.mymodules.exception.NoSuchusersExtException {

		return getPersistence().findByOfficeId_Last(
			officeId, orderByComparator);
	}

	/**
	 * Returns the last users ext in the ordered set where officeId = &#63;.
	 *
	 * @param officeId the office ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public static usersExt fetchByOfficeId_Last(
		String officeId, OrderByComparator<usersExt> orderByComparator) {

		return getPersistence().fetchByOfficeId_Last(
			officeId, orderByComparator);
	}

	/**
	 * Returns the users exts before and after the current users ext in the ordered set where officeId = &#63;.
	 *
	 * @param usersExtId the primary key of the current users ext
	 * @param officeId the office ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next users ext
	 * @throws NoSuchusersExtException if a users ext with the primary key could not be found
	 */
	public static usersExt[] findByOfficeId_PrevAndNext(
			long usersExtId, String officeId,
			OrderByComparator<usersExt> orderByComparator)
		throws com.liferay.service.mymodules.exception.NoSuchusersExtException {

		return getPersistence().findByOfficeId_PrevAndNext(
			usersExtId, officeId, orderByComparator);
	}

	/**
	 * Removes all the users exts where officeId = &#63; from the database.
	 *
	 * @param officeId the office ID
	 */
	public static void removeByOfficeId(String officeId) {
		getPersistence().removeByOfficeId(officeId);
	}

	/**
	 * Returns the number of users exts where officeId = &#63;.
	 *
	 * @param officeId the office ID
	 * @return the number of matching users exts
	 */
	public static int countByOfficeId(String officeId) {
		return getPersistence().countByOfficeId(officeId);
	}

	/**
	 * Returns all the users exts where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching users exts
	 */
	public static List<usersExt> findByUserId(long userId) {
		return getPersistence().findByUserId(userId);
	}

	/**
	 * Returns a range of all the users exts where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @return the range of matching users exts
	 */
	public static List<usersExt> findByUserId(long userId, int start, int end) {
		return getPersistence().findByUserId(userId, start, end);
	}

	/**
	 * Returns an ordered range of all the users exts where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByUserId(long, int, int, OrderByComparator)}
	 * @param userId the user ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching users exts
	 */
	@Deprecated
	public static List<usersExt> findByUserId(
		long userId, int start, int end,
		OrderByComparator<usersExt> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUserId(
			userId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns an ordered range of all the users exts where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching users exts
	 */
	public static List<usersExt> findByUserId(
		long userId, int start, int end,
		OrderByComparator<usersExt> orderByComparator) {

		return getPersistence().findByUserId(
			userId, start, end, orderByComparator);
	}

	/**
	 * Returns the first users ext in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public static usersExt findByUserId_First(
			long userId, OrderByComparator<usersExt> orderByComparator)
		throws com.liferay.service.mymodules.exception.NoSuchusersExtException {

		return getPersistence().findByUserId_First(userId, orderByComparator);
	}

	/**
	 * Returns the first users ext in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public static usersExt fetchByUserId_First(
		long userId, OrderByComparator<usersExt> orderByComparator) {

		return getPersistence().fetchByUserId_First(userId, orderByComparator);
	}

	/**
	 * Returns the last users ext in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public static usersExt findByUserId_Last(
			long userId, OrderByComparator<usersExt> orderByComparator)
		throws com.liferay.service.mymodules.exception.NoSuchusersExtException {

		return getPersistence().findByUserId_Last(userId, orderByComparator);
	}

	/**
	 * Returns the last users ext in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public static usersExt fetchByUserId_Last(
		long userId, OrderByComparator<usersExt> orderByComparator) {

		return getPersistence().fetchByUserId_Last(userId, orderByComparator);
	}

	/**
	 * Returns the users exts before and after the current users ext in the ordered set where userId = &#63;.
	 *
	 * @param usersExtId the primary key of the current users ext
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next users ext
	 * @throws NoSuchusersExtException if a users ext with the primary key could not be found
	 */
	public static usersExt[] findByUserId_PrevAndNext(
			long usersExtId, long userId,
			OrderByComparator<usersExt> orderByComparator)
		throws com.liferay.service.mymodules.exception.NoSuchusersExtException {

		return getPersistence().findByUserId_PrevAndNext(
			usersExtId, userId, orderByComparator);
	}

	/**
	 * Removes all the users exts where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 */
	public static void removeByUserId(long userId) {
		getPersistence().removeByUserId(userId);
	}

	/**
	 * Returns the number of users exts where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching users exts
	 */
	public static int countByUserId(long userId) {
		return getPersistence().countByUserId(userId);
	}

	/**
	 * Returns all the users exts where userName = &#63;.
	 *
	 * @param userName the user name
	 * @return the matching users exts
	 */
	public static List<usersExt> findByUserName(String userName) {
		return getPersistence().findByUserName(userName);
	}

	/**
	 * Returns a range of all the users exts where userName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userName the user name
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @return the range of matching users exts
	 */
	public static List<usersExt> findByUserName(
		String userName, int start, int end) {

		return getPersistence().findByUserName(userName, start, end);
	}

	/**
	 * Returns an ordered range of all the users exts where userName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByUserName(String, int, int, OrderByComparator)}
	 * @param userName the user name
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching users exts
	 */
	@Deprecated
	public static List<usersExt> findByUserName(
		String userName, int start, int end,
		OrderByComparator<usersExt> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUserName(
			userName, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns an ordered range of all the users exts where userName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userName the user name
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching users exts
	 */
	public static List<usersExt> findByUserName(
		String userName, int start, int end,
		OrderByComparator<usersExt> orderByComparator) {

		return getPersistence().findByUserName(
			userName, start, end, orderByComparator);
	}

	/**
	 * Returns the first users ext in the ordered set where userName = &#63;.
	 *
	 * @param userName the user name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public static usersExt findByUserName_First(
			String userName, OrderByComparator<usersExt> orderByComparator)
		throws com.liferay.service.mymodules.exception.NoSuchusersExtException {

		return getPersistence().findByUserName_First(
			userName, orderByComparator);
	}

	/**
	 * Returns the first users ext in the ordered set where userName = &#63;.
	 *
	 * @param userName the user name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public static usersExt fetchByUserName_First(
		String userName, OrderByComparator<usersExt> orderByComparator) {

		return getPersistence().fetchByUserName_First(
			userName, orderByComparator);
	}

	/**
	 * Returns the last users ext in the ordered set where userName = &#63;.
	 *
	 * @param userName the user name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public static usersExt findByUserName_Last(
			String userName, OrderByComparator<usersExt> orderByComparator)
		throws com.liferay.service.mymodules.exception.NoSuchusersExtException {

		return getPersistence().findByUserName_Last(
			userName, orderByComparator);
	}

	/**
	 * Returns the last users ext in the ordered set where userName = &#63;.
	 *
	 * @param userName the user name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public static usersExt fetchByUserName_Last(
		String userName, OrderByComparator<usersExt> orderByComparator) {

		return getPersistence().fetchByUserName_Last(
			userName, orderByComparator);
	}

	/**
	 * Returns the users exts before and after the current users ext in the ordered set where userName = &#63;.
	 *
	 * @param usersExtId the primary key of the current users ext
	 * @param userName the user name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next users ext
	 * @throws NoSuchusersExtException if a users ext with the primary key could not be found
	 */
	public static usersExt[] findByUserName_PrevAndNext(
			long usersExtId, String userName,
			OrderByComparator<usersExt> orderByComparator)
		throws com.liferay.service.mymodules.exception.NoSuchusersExtException {

		return getPersistence().findByUserName_PrevAndNext(
			usersExtId, userName, orderByComparator);
	}

	/**
	 * Removes all the users exts where userName = &#63; from the database.
	 *
	 * @param userName the user name
	 */
	public static void removeByUserName(String userName) {
		getPersistence().removeByUserName(userName);
	}

	/**
	 * Returns the number of users exts where userName = &#63;.
	 *
	 * @param userName the user name
	 * @return the number of matching users exts
	 */
	public static int countByUserName(String userName) {
		return getPersistence().countByUserName(userName);
	}

	/**
	 * Returns all the users exts where designation = &#63;.
	 *
	 * @param designation the designation
	 * @return the matching users exts
	 */
	public static List<usersExt> findByDesignation(String designation) {
		return getPersistence().findByDesignation(designation);
	}

	/**
	 * Returns a range of all the users exts where designation = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param designation the designation
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @return the range of matching users exts
	 */
	public static List<usersExt> findByDesignation(
		String designation, int start, int end) {

		return getPersistence().findByDesignation(designation, start, end);
	}

	/**
	 * Returns an ordered range of all the users exts where designation = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByDesignation(String, int, int, OrderByComparator)}
	 * @param designation the designation
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching users exts
	 */
	@Deprecated
	public static List<usersExt> findByDesignation(
		String designation, int start, int end,
		OrderByComparator<usersExt> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByDesignation(
			designation, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns an ordered range of all the users exts where designation = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param designation the designation
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching users exts
	 */
	public static List<usersExt> findByDesignation(
		String designation, int start, int end,
		OrderByComparator<usersExt> orderByComparator) {

		return getPersistence().findByDesignation(
			designation, start, end, orderByComparator);
	}

	/**
	 * Returns the first users ext in the ordered set where designation = &#63;.
	 *
	 * @param designation the designation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public static usersExt findByDesignation_First(
			String designation, OrderByComparator<usersExt> orderByComparator)
		throws com.liferay.service.mymodules.exception.NoSuchusersExtException {

		return getPersistence().findByDesignation_First(
			designation, orderByComparator);
	}

	/**
	 * Returns the first users ext in the ordered set where designation = &#63;.
	 *
	 * @param designation the designation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public static usersExt fetchByDesignation_First(
		String designation, OrderByComparator<usersExt> orderByComparator) {

		return getPersistence().fetchByDesignation_First(
			designation, orderByComparator);
	}

	/**
	 * Returns the last users ext in the ordered set where designation = &#63;.
	 *
	 * @param designation the designation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public static usersExt findByDesignation_Last(
			String designation, OrderByComparator<usersExt> orderByComparator)
		throws com.liferay.service.mymodules.exception.NoSuchusersExtException {

		return getPersistence().findByDesignation_Last(
			designation, orderByComparator);
	}

	/**
	 * Returns the last users ext in the ordered set where designation = &#63;.
	 *
	 * @param designation the designation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public static usersExt fetchByDesignation_Last(
		String designation, OrderByComparator<usersExt> orderByComparator) {

		return getPersistence().fetchByDesignation_Last(
			designation, orderByComparator);
	}

	/**
	 * Returns the users exts before and after the current users ext in the ordered set where designation = &#63;.
	 *
	 * @param usersExtId the primary key of the current users ext
	 * @param designation the designation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next users ext
	 * @throws NoSuchusersExtException if a users ext with the primary key could not be found
	 */
	public static usersExt[] findByDesignation_PrevAndNext(
			long usersExtId, String designation,
			OrderByComparator<usersExt> orderByComparator)
		throws com.liferay.service.mymodules.exception.NoSuchusersExtException {

		return getPersistence().findByDesignation_PrevAndNext(
			usersExtId, designation, orderByComparator);
	}

	/**
	 * Removes all the users exts where designation = &#63; from the database.
	 *
	 * @param designation the designation
	 */
	public static void removeByDesignation(String designation) {
		getPersistence().removeByDesignation(designation);
	}

	/**
	 * Returns the number of users exts where designation = &#63;.
	 *
	 * @param designation the designation
	 * @return the number of matching users exts
	 */
	public static int countByDesignation(String designation) {
		return getPersistence().countByDesignation(designation);
	}

	/**
	 * Caches the users ext in the entity cache if it is enabled.
	 *
	 * @param usersExt the users ext
	 */
	public static void cacheResult(usersExt usersExt) {
		getPersistence().cacheResult(usersExt);
	}

	/**
	 * Caches the users exts in the entity cache if it is enabled.
	 *
	 * @param usersExts the users exts
	 */
	public static void cacheResult(List<usersExt> usersExts) {
		getPersistence().cacheResult(usersExts);
	}

	/**
	 * Creates a new users ext with the primary key. Does not add the users ext to the database.
	 *
	 * @param usersExtId the primary key for the new users ext
	 * @return the new users ext
	 */
	public static usersExt create(long usersExtId) {
		return getPersistence().create(usersExtId);
	}

	/**
	 * Removes the users ext with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param usersExtId the primary key of the users ext
	 * @return the users ext that was removed
	 * @throws NoSuchusersExtException if a users ext with the primary key could not be found
	 */
	public static usersExt remove(long usersExtId)
		throws com.liferay.service.mymodules.exception.NoSuchusersExtException {

		return getPersistence().remove(usersExtId);
	}

	public static usersExt updateImpl(usersExt usersExt) {
		return getPersistence().updateImpl(usersExt);
	}

	/**
	 * Returns the users ext with the primary key or throws a <code>NoSuchusersExtException</code> if it could not be found.
	 *
	 * @param usersExtId the primary key of the users ext
	 * @return the users ext
	 * @throws NoSuchusersExtException if a users ext with the primary key could not be found
	 */
	public static usersExt findByPrimaryKey(long usersExtId)
		throws com.liferay.service.mymodules.exception.NoSuchusersExtException {

		return getPersistence().findByPrimaryKey(usersExtId);
	}

	/**
	 * Returns the users ext with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param usersExtId the primary key of the users ext
	 * @return the users ext, or <code>null</code> if a users ext with the primary key could not be found
	 */
	public static usersExt fetchByPrimaryKey(long usersExtId) {
		return getPersistence().fetchByPrimaryKey(usersExtId);
	}

	/**
	 * Returns all the users exts.
	 *
	 * @return the users exts
	 */
	public static List<usersExt> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the users exts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @return the range of users exts
	 */
	public static List<usersExt> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the users exts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findAll(int, int, OrderByComparator)}
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of users exts
	 */
	@Deprecated
	public static List<usersExt> findAll(
		int start, int end, OrderByComparator<usersExt> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns an ordered range of all the users exts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of users exts
	 */
	public static List<usersExt> findAll(
		int start, int end, OrderByComparator<usersExt> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Removes all the users exts from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of users exts.
	 *
	 * @return the number of users exts
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static usersExtPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<usersExtPersistence, usersExtPersistence>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(usersExtPersistence.class);

		ServiceTracker<usersExtPersistence, usersExtPersistence>
			serviceTracker =
				new ServiceTracker<usersExtPersistence, usersExtPersistence>(
					bundle.getBundleContext(), usersExtPersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}