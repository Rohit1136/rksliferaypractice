/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.service.mymodules.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.service.mymodules.exception.NoSuchusersExtException;
import com.liferay.service.mymodules.model.usersExt;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the users ext service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author rohit
 * @see usersExtUtil
 * @generated
 */
@ProviderType
public interface usersExtPersistence extends BasePersistence<usersExt> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link usersExtUtil} to access the users ext persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the users exts where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching users exts
	 */
	public java.util.List<usersExt> findByUuid(String uuid);

	/**
	 * Returns a range of all the users exts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @return the range of matching users exts
	 */
	public java.util.List<usersExt> findByUuid(String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the users exts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByUuid(String, int, int, OrderByComparator)}
	 * @param uuid the uuid
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching users exts
	 */
	@Deprecated
	public java.util.List<usersExt> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<usersExt> orderByComparator, boolean useFinderCache);

	/**
	 * Returns an ordered range of all the users exts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching users exts
	 */
	public java.util.List<usersExt> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<usersExt> orderByComparator);

	/**
	 * Returns the first users ext in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public usersExt findByUuid_First(
			String uuid, OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException;

	/**
	 * Returns the first users ext in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public usersExt fetchByUuid_First(
		String uuid, OrderByComparator<usersExt> orderByComparator);

	/**
	 * Returns the last users ext in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public usersExt findByUuid_Last(
			String uuid, OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException;

	/**
	 * Returns the last users ext in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public usersExt fetchByUuid_Last(
		String uuid, OrderByComparator<usersExt> orderByComparator);

	/**
	 * Returns the users exts before and after the current users ext in the ordered set where uuid = &#63;.
	 *
	 * @param usersExtId the primary key of the current users ext
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next users ext
	 * @throws NoSuchusersExtException if a users ext with the primary key could not be found
	 */
	public usersExt[] findByUuid_PrevAndNext(
			long usersExtId, String uuid,
			OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException;

	/**
	 * Removes all the users exts where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of users exts where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching users exts
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the users ext where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchusersExtException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public usersExt findByUUID_G(String uuid, long groupId)
		throws NoSuchusersExtException;

	/**
	 * Returns the users ext where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #fetchByUUID_G(String,long)}
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	@Deprecated
	public usersExt fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Returns the users ext where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public usersExt fetchByUUID_G(String uuid, long groupId);

	/**
	 * Removes the users ext where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the users ext that was removed
	 */
	public usersExt removeByUUID_G(String uuid, long groupId)
		throws NoSuchusersExtException;

	/**
	 * Returns the number of users exts where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching users exts
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the users exts where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching users exts
	 */
	public java.util.List<usersExt> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the users exts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @return the range of matching users exts
	 */
	public java.util.List<usersExt> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the users exts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByUuid_C(String,long, int, int, OrderByComparator)}
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching users exts
	 */
	@Deprecated
	public java.util.List<usersExt> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<usersExt> orderByComparator, boolean useFinderCache);

	/**
	 * Returns an ordered range of all the users exts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching users exts
	 */
	public java.util.List<usersExt> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<usersExt> orderByComparator);

	/**
	 * Returns the first users ext in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public usersExt findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException;

	/**
	 * Returns the first users ext in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public usersExt fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<usersExt> orderByComparator);

	/**
	 * Returns the last users ext in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public usersExt findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException;

	/**
	 * Returns the last users ext in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public usersExt fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<usersExt> orderByComparator);

	/**
	 * Returns the users exts before and after the current users ext in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param usersExtId the primary key of the current users ext
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next users ext
	 * @throws NoSuchusersExtException if a users ext with the primary key could not be found
	 */
	public usersExt[] findByUuid_C_PrevAndNext(
			long usersExtId, String uuid, long companyId,
			OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException;

	/**
	 * Removes all the users exts where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of users exts where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching users exts
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the users exts where usersExtId = &#63;.
	 *
	 * @param usersExtId the users ext ID
	 * @return the matching users exts
	 */
	public java.util.List<usersExt> findByUserExtId(long usersExtId);

	/**
	 * Returns a range of all the users exts where usersExtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param usersExtId the users ext ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @return the range of matching users exts
	 */
	public java.util.List<usersExt> findByUserExtId(
		long usersExtId, int start, int end);

	/**
	 * Returns an ordered range of all the users exts where usersExtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByUserExtId(long, int, int, OrderByComparator)}
	 * @param usersExtId the users ext ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching users exts
	 */
	@Deprecated
	public java.util.List<usersExt> findByUserExtId(
		long usersExtId, int start, int end,
		OrderByComparator<usersExt> orderByComparator, boolean useFinderCache);

	/**
	 * Returns an ordered range of all the users exts where usersExtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param usersExtId the users ext ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching users exts
	 */
	public java.util.List<usersExt> findByUserExtId(
		long usersExtId, int start, int end,
		OrderByComparator<usersExt> orderByComparator);

	/**
	 * Returns the first users ext in the ordered set where usersExtId = &#63;.
	 *
	 * @param usersExtId the users ext ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public usersExt findByUserExtId_First(
			long usersExtId, OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException;

	/**
	 * Returns the first users ext in the ordered set where usersExtId = &#63;.
	 *
	 * @param usersExtId the users ext ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public usersExt fetchByUserExtId_First(
		long usersExtId, OrderByComparator<usersExt> orderByComparator);

	/**
	 * Returns the last users ext in the ordered set where usersExtId = &#63;.
	 *
	 * @param usersExtId the users ext ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public usersExt findByUserExtId_Last(
			long usersExtId, OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException;

	/**
	 * Returns the last users ext in the ordered set where usersExtId = &#63;.
	 *
	 * @param usersExtId the users ext ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public usersExt fetchByUserExtId_Last(
		long usersExtId, OrderByComparator<usersExt> orderByComparator);

	/**
	 * Removes all the users exts where usersExtId = &#63; from the database.
	 *
	 * @param usersExtId the users ext ID
	 */
	public void removeByUserExtId(long usersExtId);

	/**
	 * Returns the number of users exts where usersExtId = &#63;.
	 *
	 * @param usersExtId the users ext ID
	 * @return the number of matching users exts
	 */
	public int countByUserExtId(long usersExtId);

	/**
	 * Returns all the users exts where officeId = &#63;.
	 *
	 * @param officeId the office ID
	 * @return the matching users exts
	 */
	public java.util.List<usersExt> findByOfficeId(String officeId);

	/**
	 * Returns a range of all the users exts where officeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param officeId the office ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @return the range of matching users exts
	 */
	public java.util.List<usersExt> findByOfficeId(
		String officeId, int start, int end);

	/**
	 * Returns an ordered range of all the users exts where officeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByOfficeId(String, int, int, OrderByComparator)}
	 * @param officeId the office ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching users exts
	 */
	@Deprecated
	public java.util.List<usersExt> findByOfficeId(
		String officeId, int start, int end,
		OrderByComparator<usersExt> orderByComparator, boolean useFinderCache);

	/**
	 * Returns an ordered range of all the users exts where officeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param officeId the office ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching users exts
	 */
	public java.util.List<usersExt> findByOfficeId(
		String officeId, int start, int end,
		OrderByComparator<usersExt> orderByComparator);

	/**
	 * Returns the first users ext in the ordered set where officeId = &#63;.
	 *
	 * @param officeId the office ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public usersExt findByOfficeId_First(
			String officeId, OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException;

	/**
	 * Returns the first users ext in the ordered set where officeId = &#63;.
	 *
	 * @param officeId the office ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public usersExt fetchByOfficeId_First(
		String officeId, OrderByComparator<usersExt> orderByComparator);

	/**
	 * Returns the last users ext in the ordered set where officeId = &#63;.
	 *
	 * @param officeId the office ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public usersExt findByOfficeId_Last(
			String officeId, OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException;

	/**
	 * Returns the last users ext in the ordered set where officeId = &#63;.
	 *
	 * @param officeId the office ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public usersExt fetchByOfficeId_Last(
		String officeId, OrderByComparator<usersExt> orderByComparator);

	/**
	 * Returns the users exts before and after the current users ext in the ordered set where officeId = &#63;.
	 *
	 * @param usersExtId the primary key of the current users ext
	 * @param officeId the office ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next users ext
	 * @throws NoSuchusersExtException if a users ext with the primary key could not be found
	 */
	public usersExt[] findByOfficeId_PrevAndNext(
			long usersExtId, String officeId,
			OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException;

	/**
	 * Removes all the users exts where officeId = &#63; from the database.
	 *
	 * @param officeId the office ID
	 */
	public void removeByOfficeId(String officeId);

	/**
	 * Returns the number of users exts where officeId = &#63;.
	 *
	 * @param officeId the office ID
	 * @return the number of matching users exts
	 */
	public int countByOfficeId(String officeId);

	/**
	 * Returns all the users exts where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching users exts
	 */
	public java.util.List<usersExt> findByUserId(long userId);

	/**
	 * Returns a range of all the users exts where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @return the range of matching users exts
	 */
	public java.util.List<usersExt> findByUserId(
		long userId, int start, int end);

	/**
	 * Returns an ordered range of all the users exts where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByUserId(long, int, int, OrderByComparator)}
	 * @param userId the user ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching users exts
	 */
	@Deprecated
	public java.util.List<usersExt> findByUserId(
		long userId, int start, int end,
		OrderByComparator<usersExt> orderByComparator, boolean useFinderCache);

	/**
	 * Returns an ordered range of all the users exts where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching users exts
	 */
	public java.util.List<usersExt> findByUserId(
		long userId, int start, int end,
		OrderByComparator<usersExt> orderByComparator);

	/**
	 * Returns the first users ext in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public usersExt findByUserId_First(
			long userId, OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException;

	/**
	 * Returns the first users ext in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public usersExt fetchByUserId_First(
		long userId, OrderByComparator<usersExt> orderByComparator);

	/**
	 * Returns the last users ext in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public usersExt findByUserId_Last(
			long userId, OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException;

	/**
	 * Returns the last users ext in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public usersExt fetchByUserId_Last(
		long userId, OrderByComparator<usersExt> orderByComparator);

	/**
	 * Returns the users exts before and after the current users ext in the ordered set where userId = &#63;.
	 *
	 * @param usersExtId the primary key of the current users ext
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next users ext
	 * @throws NoSuchusersExtException if a users ext with the primary key could not be found
	 */
	public usersExt[] findByUserId_PrevAndNext(
			long usersExtId, long userId,
			OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException;

	/**
	 * Removes all the users exts where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 */
	public void removeByUserId(long userId);

	/**
	 * Returns the number of users exts where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching users exts
	 */
	public int countByUserId(long userId);

	/**
	 * Returns all the users exts where userName = &#63;.
	 *
	 * @param userName the user name
	 * @return the matching users exts
	 */
	public java.util.List<usersExt> findByUserName(String userName);

	/**
	 * Returns a range of all the users exts where userName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userName the user name
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @return the range of matching users exts
	 */
	public java.util.List<usersExt> findByUserName(
		String userName, int start, int end);

	/**
	 * Returns an ordered range of all the users exts where userName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByUserName(String, int, int, OrderByComparator)}
	 * @param userName the user name
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching users exts
	 */
	@Deprecated
	public java.util.List<usersExt> findByUserName(
		String userName, int start, int end,
		OrderByComparator<usersExt> orderByComparator, boolean useFinderCache);

	/**
	 * Returns an ordered range of all the users exts where userName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userName the user name
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching users exts
	 */
	public java.util.List<usersExt> findByUserName(
		String userName, int start, int end,
		OrderByComparator<usersExt> orderByComparator);

	/**
	 * Returns the first users ext in the ordered set where userName = &#63;.
	 *
	 * @param userName the user name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public usersExt findByUserName_First(
			String userName, OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException;

	/**
	 * Returns the first users ext in the ordered set where userName = &#63;.
	 *
	 * @param userName the user name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public usersExt fetchByUserName_First(
		String userName, OrderByComparator<usersExt> orderByComparator);

	/**
	 * Returns the last users ext in the ordered set where userName = &#63;.
	 *
	 * @param userName the user name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public usersExt findByUserName_Last(
			String userName, OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException;

	/**
	 * Returns the last users ext in the ordered set where userName = &#63;.
	 *
	 * @param userName the user name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public usersExt fetchByUserName_Last(
		String userName, OrderByComparator<usersExt> orderByComparator);

	/**
	 * Returns the users exts before and after the current users ext in the ordered set where userName = &#63;.
	 *
	 * @param usersExtId the primary key of the current users ext
	 * @param userName the user name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next users ext
	 * @throws NoSuchusersExtException if a users ext with the primary key could not be found
	 */
	public usersExt[] findByUserName_PrevAndNext(
			long usersExtId, String userName,
			OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException;

	/**
	 * Removes all the users exts where userName = &#63; from the database.
	 *
	 * @param userName the user name
	 */
	public void removeByUserName(String userName);

	/**
	 * Returns the number of users exts where userName = &#63;.
	 *
	 * @param userName the user name
	 * @return the number of matching users exts
	 */
	public int countByUserName(String userName);

	/**
	 * Returns all the users exts where designation = &#63;.
	 *
	 * @param designation the designation
	 * @return the matching users exts
	 */
	public java.util.List<usersExt> findByDesignation(String designation);

	/**
	 * Returns a range of all the users exts where designation = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param designation the designation
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @return the range of matching users exts
	 */
	public java.util.List<usersExt> findByDesignation(
		String designation, int start, int end);

	/**
	 * Returns an ordered range of all the users exts where designation = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByDesignation(String, int, int, OrderByComparator)}
	 * @param designation the designation
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching users exts
	 */
	@Deprecated
	public java.util.List<usersExt> findByDesignation(
		String designation, int start, int end,
		OrderByComparator<usersExt> orderByComparator, boolean useFinderCache);

	/**
	 * Returns an ordered range of all the users exts where designation = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param designation the designation
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching users exts
	 */
	public java.util.List<usersExt> findByDesignation(
		String designation, int start, int end,
		OrderByComparator<usersExt> orderByComparator);

	/**
	 * Returns the first users ext in the ordered set where designation = &#63;.
	 *
	 * @param designation the designation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public usersExt findByDesignation_First(
			String designation, OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException;

	/**
	 * Returns the first users ext in the ordered set where designation = &#63;.
	 *
	 * @param designation the designation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public usersExt fetchByDesignation_First(
		String designation, OrderByComparator<usersExt> orderByComparator);

	/**
	 * Returns the last users ext in the ordered set where designation = &#63;.
	 *
	 * @param designation the designation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	public usersExt findByDesignation_Last(
			String designation, OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException;

	/**
	 * Returns the last users ext in the ordered set where designation = &#63;.
	 *
	 * @param designation the designation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public usersExt fetchByDesignation_Last(
		String designation, OrderByComparator<usersExt> orderByComparator);

	/**
	 * Returns the users exts before and after the current users ext in the ordered set where designation = &#63;.
	 *
	 * @param usersExtId the primary key of the current users ext
	 * @param designation the designation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next users ext
	 * @throws NoSuchusersExtException if a users ext with the primary key could not be found
	 */
	public usersExt[] findByDesignation_PrevAndNext(
			long usersExtId, String designation,
			OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException;

	/**
	 * Removes all the users exts where designation = &#63; from the database.
	 *
	 * @param designation the designation
	 */
	public void removeByDesignation(String designation);

	/**
	 * Returns the number of users exts where designation = &#63;.
	 *
	 * @param designation the designation
	 * @return the number of matching users exts
	 */
	public int countByDesignation(String designation);

	/**
	 * Caches the users ext in the entity cache if it is enabled.
	 *
	 * @param usersExt the users ext
	 */
	public void cacheResult(usersExt usersExt);

	/**
	 * Caches the users exts in the entity cache if it is enabled.
	 *
	 * @param usersExts the users exts
	 */
	public void cacheResult(java.util.List<usersExt> usersExts);

	/**
	 * Creates a new users ext with the primary key. Does not add the users ext to the database.
	 *
	 * @param usersExtId the primary key for the new users ext
	 * @return the new users ext
	 */
	public usersExt create(long usersExtId);

	/**
	 * Removes the users ext with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param usersExtId the primary key of the users ext
	 * @return the users ext that was removed
	 * @throws NoSuchusersExtException if a users ext with the primary key could not be found
	 */
	public usersExt remove(long usersExtId) throws NoSuchusersExtException;

	public usersExt updateImpl(usersExt usersExt);

	/**
	 * Returns the users ext with the primary key or throws a <code>NoSuchusersExtException</code> if it could not be found.
	 *
	 * @param usersExtId the primary key of the users ext
	 * @return the users ext
	 * @throws NoSuchusersExtException if a users ext with the primary key could not be found
	 */
	public usersExt findByPrimaryKey(long usersExtId)
		throws NoSuchusersExtException;

	/**
	 * Returns the users ext with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param usersExtId the primary key of the users ext
	 * @return the users ext, or <code>null</code> if a users ext with the primary key could not be found
	 */
	public usersExt fetchByPrimaryKey(long usersExtId);

	/**
	 * Returns all the users exts.
	 *
	 * @return the users exts
	 */
	public java.util.List<usersExt> findAll();

	/**
	 * Returns a range of all the users exts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @return the range of users exts
	 */
	public java.util.List<usersExt> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the users exts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findAll(int, int, OrderByComparator)}
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of users exts
	 */
	@Deprecated
	public java.util.List<usersExt> findAll(
		int start, int end, OrderByComparator<usersExt> orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns an ordered range of all the users exts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of users exts
	 */
	public java.util.List<usersExt> findAll(
		int start, int end, OrderByComparator<usersExt> orderByComparator);

	/**
	 * Removes all the users exts from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of users exts.
	 *
	 * @return the number of users exts
	 */
	public int countAll();

}