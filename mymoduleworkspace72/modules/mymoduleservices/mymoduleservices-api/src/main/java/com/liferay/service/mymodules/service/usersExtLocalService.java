/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.service.mymodules.service;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.service.mymodules.model.usersExt;

import java.io.Serializable;

import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the local service interface for usersExt. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author rohit
 * @see usersExtLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface usersExtLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link usersExtLocalServiceUtil} to access the users ext local service. Add custom service methods to <code>com.liferay.service.mymodules.service.impl.usersExtLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */

	/**
	 * Adds the users ext to the database. Also notifies the appropriate model listeners.
	 *
	 * @param usersExt the users ext
	 * @return the users ext that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public usersExt addusersExt(usersExt usersExt);

	/**
	 * Creates a new users ext with the primary key. Does not add the users ext to the database.
	 *
	 * @param usersExtId the primary key for the new users ext
	 * @return the new users ext
	 */
	@Transactional(enabled = false)
	public usersExt createusersExt(long usersExtId);

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	/**
	 * Deletes the users ext with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param usersExtId the primary key of the users ext
	 * @return the users ext that was removed
	 * @throws PortalException if a users ext with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public usersExt deleteusersExt(long usersExtId) throws PortalException;

	/**
	 * Deletes the users ext from the database. Also notifies the appropriate model listeners.
	 *
	 * @param usersExt the users ext
	 * @return the users ext that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public usersExt deleteusersExt(usersExt usersExt);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.liferay.service.mymodules.model.impl.usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.liferay.service.mymodules.model.impl.usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public usersExt fetchusersExt(long usersExtId);

	/**
	 * Returns the users ext matching the UUID and group.
	 *
	 * @param uuid the users ext's UUID
	 * @param groupId the primary key of the group
	 * @return the matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public usersExt fetchusersExtByUuidAndGroupId(String uuid, long groupId);

	public List<usersExt> findByDesignation(String designation);

	public List<usersExt> findByOfficeId(String officeId);

	public List<usersExt> findByUserExtId(long userExtId);

	public List<usersExt> findByUserId(long userId);

	public List<usersExt> findByUserName(String userName);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Returns the users ext with the primary key.
	 *
	 * @param usersExtId the primary key of the users ext
	 * @return the users ext
	 * @throws PortalException if a users ext with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public usersExt getusersExt(long usersExtId) throws PortalException;

	/**
	 * Returns the users ext matching the UUID and group.
	 *
	 * @param uuid the users ext's UUID
	 * @param groupId the primary key of the group
	 * @return the matching users ext
	 * @throws PortalException if a matching users ext could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public usersExt getusersExtByUuidAndGroupId(String uuid, long groupId)
		throws PortalException;

	/**
	 * Returns a range of all the users exts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.liferay.service.mymodules.model.impl.usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @return the range of users exts
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<usersExt> getusersExts(int start, int end);

	/**
	 * Returns all the users exts matching the UUID and company.
	 *
	 * @param uuid the UUID of the users exts
	 * @param companyId the primary key of the company
	 * @return the matching users exts, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<usersExt> getusersExtsByUuidAndCompanyId(
		String uuid, long companyId);

	/**
	 * Returns a range of users exts matching the UUID and company.
	 *
	 * @param uuid the UUID of the users exts
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching users exts, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<usersExt> getusersExtsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<usersExt> orderByComparator);

	/**
	 * Returns the number of users exts.
	 *
	 * @return the number of users exts
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getusersExtsCount();

	/**
	 * Updates the users ext in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param usersExt the users ext
	 * @return the users ext that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public usersExt updateusersExt(usersExt usersExt);

}