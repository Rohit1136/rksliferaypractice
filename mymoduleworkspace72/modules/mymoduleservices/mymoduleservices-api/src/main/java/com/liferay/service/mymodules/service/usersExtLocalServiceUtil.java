/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.service.mymodules.service;

import org.osgi.annotation.versioning.ProviderType;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for usersExt. This utility wraps
 * <code>com.liferay.service.mymodules.service.impl.usersExtLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author rohit
 * @see usersExtLocalService
 * @generated
 */
@ProviderType
public class usersExtLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.liferay.service.mymodules.service.impl.usersExtLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the users ext to the database. Also notifies the appropriate model listeners.
	 *
	 * @param usersExt the users ext
	 * @return the users ext that was added
	 */
	public static com.liferay.service.mymodules.model.usersExt addusersExt(
		com.liferay.service.mymodules.model.usersExt usersExt) {

		return getService().addusersExt(usersExt);
	}

	/**
	 * Creates a new users ext with the primary key. Does not add the users ext to the database.
	 *
	 * @param usersExtId the primary key for the new users ext
	 * @return the new users ext
	 */
	public static com.liferay.service.mymodules.model.usersExt createusersExt(
		long usersExtId) {

		return getService().createusersExt(usersExtId);
	}

	/**
	 * @throws PortalException
	 */
	public static com.liferay.portal.kernel.model.PersistedModel
			deletePersistedModel(
				com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the users ext with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param usersExtId the primary key of the users ext
	 * @return the users ext that was removed
	 * @throws PortalException if a users ext with the primary key could not be found
	 */
	public static com.liferay.service.mymodules.model.usersExt deleteusersExt(
			long usersExtId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deleteusersExt(usersExtId);
	}

	/**
	 * Deletes the users ext from the database. Also notifies the appropriate model listeners.
	 *
	 * @param usersExt the users ext
	 * @return the users ext that was removed
	 */
	public static com.liferay.service.mymodules.model.usersExt deleteusersExt(
		com.liferay.service.mymodules.model.usersExt usersExt) {

		return getService().deleteusersExt(usersExt);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery
		dynamicQuery() {

		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.liferay.service.mymodules.model.impl.usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.liferay.service.mymodules.model.impl.usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static com.liferay.service.mymodules.model.usersExt fetchusersExt(
		long usersExtId) {

		return getService().fetchusersExt(usersExtId);
	}

	/**
	 * Returns the users ext matching the UUID and group.
	 *
	 * @param uuid the users ext's UUID
	 * @param groupId the primary key of the group
	 * @return the matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	public static com.liferay.service.mymodules.model.usersExt
		fetchusersExtByUuidAndGroupId(String uuid, long groupId) {

		return getService().fetchusersExtByUuidAndGroupId(uuid, groupId);
	}

	public static java.util.List<com.liferay.service.mymodules.model.usersExt>
		findByDesignation(String designation) {

		return getService().findByDesignation(designation);
	}

	public static java.util.List<com.liferay.service.mymodules.model.usersExt>
		findByOfficeId(String officeId) {

		return getService().findByOfficeId(officeId);
	}

	public static java.util.List<com.liferay.service.mymodules.model.usersExt>
		findByUserExtId(long userExtId) {

		return getService().findByUserExtId(userExtId);
	}

	public static java.util.List<com.liferay.service.mymodules.model.usersExt>
		findByUserId(long userId) {

		return getService().findByUserId(userId);
	}

	public static java.util.List<com.liferay.service.mymodules.model.usersExt>
		findByUserName(String userName) {

		return getService().findByUserName(userName);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static com.liferay.portal.kernel.model.PersistedModel
			getPersistedModel(java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the users ext with the primary key.
	 *
	 * @param usersExtId the primary key of the users ext
	 * @return the users ext
	 * @throws PortalException if a users ext with the primary key could not be found
	 */
	public static com.liferay.service.mymodules.model.usersExt getusersExt(
			long usersExtId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getusersExt(usersExtId);
	}

	/**
	 * Returns the users ext matching the UUID and group.
	 *
	 * @param uuid the users ext's UUID
	 * @param groupId the primary key of the group
	 * @return the matching users ext
	 * @throws PortalException if a matching users ext could not be found
	 */
	public static com.liferay.service.mymodules.model.usersExt
			getusersExtByUuidAndGroupId(String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getusersExtByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the users exts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.liferay.service.mymodules.model.impl.usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @return the range of users exts
	 */
	public static java.util.List<com.liferay.service.mymodules.model.usersExt>
		getusersExts(int start, int end) {

		return getService().getusersExts(start, end);
	}

	/**
	 * Returns all the users exts matching the UUID and company.
	 *
	 * @param uuid the UUID of the users exts
	 * @param companyId the primary key of the company
	 * @return the matching users exts, or an empty list if no matches were found
	 */
	public static java.util.List<com.liferay.service.mymodules.model.usersExt>
		getusersExtsByUuidAndCompanyId(String uuid, long companyId) {

		return getService().getusersExtsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of users exts matching the UUID and company.
	 *
	 * @param uuid the UUID of the users exts
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching users exts, or an empty list if no matches were found
	 */
	public static java.util.List<com.liferay.service.mymodules.model.usersExt>
		getusersExtsByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.liferay.service.mymodules.model.usersExt>
					orderByComparator) {

		return getService().getusersExtsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of users exts.
	 *
	 * @return the number of users exts
	 */
	public static int getusersExtsCount() {
		return getService().getusersExtsCount();
	}

	/**
	 * Updates the users ext in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param usersExt the users ext
	 * @return the users ext that was updated
	 */
	public static com.liferay.service.mymodules.model.usersExt updateusersExt(
		com.liferay.service.mymodules.model.usersExt usersExt) {

		return getService().updateusersExt(usersExt);
	}

	public static usersExtLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<usersExtLocalService, usersExtLocalService>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(usersExtLocalService.class);

		ServiceTracker<usersExtLocalService, usersExtLocalService>
			serviceTracker =
				new ServiceTracker<usersExtLocalService, usersExtLocalService>(
					bundle.getBundleContext(), usersExtLocalService.class,
					null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}