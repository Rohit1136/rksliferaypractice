/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.service.mymodules.model;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The extended model interface for the usersExt service. Represents a row in the &quot;rksModule_usersExt&quot; database table, with each column mapped to a property of this class.
 *
 * @author rohit
 * @see usersExtModel
 * @generated
 */
@ImplementationClassName(
	"com.liferay.service.mymodules.model.impl.usersExtImpl"
)
@ProviderType
public interface usersExt extends PersistedModel, usersExtModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to <code>com.liferay.service.mymodules.model.impl.usersExtImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<usersExt, Long> USERS_EXT_ID_ACCESSOR =
		new Accessor<usersExt, Long>() {

			@Override
			public Long get(usersExt usersExt) {
				return usersExt.getUsersExtId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<usersExt> getTypeClass() {
				return usersExt.class;
			}

		};

}