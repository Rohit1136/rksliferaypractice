/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.service.mymodules.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

/**
 * This class is used by SOAP remote services, specifically {@link com.liferay.service.mymodules.service.http.usersExtServiceSoap}.
 *
 * @author rohit
 * @generated
 */
@ProviderType
public class usersExtSoap implements Serializable {

	public static usersExtSoap toSoapModel(usersExt model) {
		usersExtSoap soapModel = new usersExtSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setUsersExtId(model.getUsersExtId());
		soapModel.setOfficeId(model.getOfficeId());
		soapModel.setOfficeName(model.getOfficeName());
		soapModel.setDesignation(model.getDesignation());
		soapModel.setOfficeAddress(model.getOfficeAddress());
		soapModel.setIsActive(model.isIsActive());
		soapModel.setSource(model.getSource());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());

		return soapModel;
	}

	public static usersExtSoap[] toSoapModels(usersExt[] models) {
		usersExtSoap[] soapModels = new usersExtSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static usersExtSoap[][] toSoapModels(usersExt[][] models) {
		usersExtSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new usersExtSoap[models.length][models[0].length];
		}
		else {
			soapModels = new usersExtSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static usersExtSoap[] toSoapModels(List<usersExt> models) {
		List<usersExtSoap> soapModels = new ArrayList<usersExtSoap>(
			models.size());

		for (usersExt model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new usersExtSoap[soapModels.size()]);
	}

	public usersExtSoap() {
	}

	public long getPrimaryKey() {
		return _usersExtId;
	}

	public void setPrimaryKey(long pk) {
		setUsersExtId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getUsersExtId() {
		return _usersExtId;
	}

	public void setUsersExtId(long usersExtId) {
		_usersExtId = usersExtId;
	}

	public String getOfficeId() {
		return _officeId;
	}

	public void setOfficeId(String officeId) {
		_officeId = officeId;
	}

	public String getOfficeName() {
		return _officeName;
	}

	public void setOfficeName(String officeName) {
		_officeName = officeName;
	}

	public String getDesignation() {
		return _designation;
	}

	public void setDesignation(String designation) {
		_designation = designation;
	}

	public String getOfficeAddress() {
		return _officeAddress;
	}

	public void setOfficeAddress(String officeAddress) {
		_officeAddress = officeAddress;
	}

	public boolean getIsActive() {
		return _isActive;
	}

	public boolean isIsActive() {
		return _isActive;
	}

	public void setIsActive(boolean isActive) {
		_isActive = isActive;
	}

	public String getSource() {
		return _source;
	}

	public void setSource(String source) {
		_source = source;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	private String _uuid;
	private long _usersExtId;
	private String _officeId;
	private String _officeName;
	private String _designation;
	private String _officeAddress;
	private boolean _isActive;
	private String _source;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;

}