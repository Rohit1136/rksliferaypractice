/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.service.mymodules.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides a wrapper for {@link usersExtService}.
 *
 * @author rohit
 * @see usersExtService
 * @generated
 */
@ProviderType
public class usersExtServiceWrapper
	implements usersExtService, ServiceWrapper<usersExtService> {

	public usersExtServiceWrapper(usersExtService usersExtService) {
		_usersExtService = usersExtService;
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _usersExtService.getOSGiServiceIdentifier();
	}

	@Override
	public usersExtService getWrappedService() {
		return _usersExtService;
	}

	@Override
	public void setWrappedService(usersExtService usersExtService) {
		_usersExtService = usersExtService;
	}

	private usersExtService _usersExtService;

}