/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.service.mymodules.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.osgi.annotation.versioning.ProviderType;

/**
 * <p>
 * This class is a wrapper for {@link usersExt}.
 * </p>
 *
 * @author rohit
 * @see usersExt
 * @generated
 */
@ProviderType
public class usersExtWrapper
	extends BaseModelWrapper<usersExt>
	implements usersExt, ModelWrapper<usersExt> {

	public usersExtWrapper(usersExt usersExt) {
		super(usersExt);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("usersExtId", getUsersExtId());
		attributes.put("officeId", getOfficeId());
		attributes.put("officeName", getOfficeName());
		attributes.put("designation", getDesignation());
		attributes.put("officeAddress", getOfficeAddress());
		attributes.put("isActive", isIsActive());
		attributes.put("source", getSource());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long usersExtId = (Long)attributes.get("usersExtId");

		if (usersExtId != null) {
			setUsersExtId(usersExtId);
		}

		String officeId = (String)attributes.get("officeId");

		if (officeId != null) {
			setOfficeId(officeId);
		}

		String officeName = (String)attributes.get("officeName");

		if (officeName != null) {
			setOfficeName(officeName);
		}

		String designation = (String)attributes.get("designation");

		if (designation != null) {
			setDesignation(designation);
		}

		String officeAddress = (String)attributes.get("officeAddress");

		if (officeAddress != null) {
			setOfficeAddress(officeAddress);
		}

		Boolean isActive = (Boolean)attributes.get("isActive");

		if (isActive != null) {
			setIsActive(isActive);
		}

		String source = (String)attributes.get("source");

		if (source != null) {
			setSource(source);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}
	}

	/**
	 * Returns the company ID of this users ext.
	 *
	 * @return the company ID of this users ext
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this users ext.
	 *
	 * @return the create date of this users ext
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the designation of this users ext.
	 *
	 * @return the designation of this users ext
	 */
	@Override
	public String getDesignation() {
		return model.getDesignation();
	}

	/**
	 * Returns the group ID of this users ext.
	 *
	 * @return the group ID of this users ext
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the is active of this users ext.
	 *
	 * @return the is active of this users ext
	 */
	@Override
	public boolean getIsActive() {
		return model.getIsActive();
	}

	/**
	 * Returns the modified date of this users ext.
	 *
	 * @return the modified date of this users ext
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the office address of this users ext.
	 *
	 * @return the office address of this users ext
	 */
	@Override
	public String getOfficeAddress() {
		return model.getOfficeAddress();
	}

	/**
	 * Returns the office ID of this users ext.
	 *
	 * @return the office ID of this users ext
	 */
	@Override
	public String getOfficeId() {
		return model.getOfficeId();
	}

	/**
	 * Returns the office name of this users ext.
	 *
	 * @return the office name of this users ext
	 */
	@Override
	public String getOfficeName() {
		return model.getOfficeName();
	}

	/**
	 * Returns the primary key of this users ext.
	 *
	 * @return the primary key of this users ext
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the source of this users ext.
	 *
	 * @return the source of this users ext
	 */
	@Override
	public String getSource() {
		return model.getSource();
	}

	/**
	 * Returns the user ID of this users ext.
	 *
	 * @return the user ID of this users ext
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this users ext.
	 *
	 * @return the user name of this users ext
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the users ext ID of this users ext.
	 *
	 * @return the users ext ID of this users ext
	 */
	@Override
	public long getUsersExtId() {
		return model.getUsersExtId();
	}

	/**
	 * Returns the user uuid of this users ext.
	 *
	 * @return the user uuid of this users ext
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this users ext.
	 *
	 * @return the uuid of this users ext
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	/**
	 * Returns <code>true</code> if this users ext is is active.
	 *
	 * @return <code>true</code> if this users ext is is active; <code>false</code> otherwise
	 */
	@Override
	public boolean isIsActive() {
		return model.isIsActive();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the company ID of this users ext.
	 *
	 * @param companyId the company ID of this users ext
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this users ext.
	 *
	 * @param createDate the create date of this users ext
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the designation of this users ext.
	 *
	 * @param designation the designation of this users ext
	 */
	@Override
	public void setDesignation(String designation) {
		model.setDesignation(designation);
	}

	/**
	 * Sets the group ID of this users ext.
	 *
	 * @param groupId the group ID of this users ext
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets whether this users ext is is active.
	 *
	 * @param isActive the is active of this users ext
	 */
	@Override
	public void setIsActive(boolean isActive) {
		model.setIsActive(isActive);
	}

	/**
	 * Sets the modified date of this users ext.
	 *
	 * @param modifiedDate the modified date of this users ext
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the office address of this users ext.
	 *
	 * @param officeAddress the office address of this users ext
	 */
	@Override
	public void setOfficeAddress(String officeAddress) {
		model.setOfficeAddress(officeAddress);
	}

	/**
	 * Sets the office ID of this users ext.
	 *
	 * @param officeId the office ID of this users ext
	 */
	@Override
	public void setOfficeId(String officeId) {
		model.setOfficeId(officeId);
	}

	/**
	 * Sets the office name of this users ext.
	 *
	 * @param officeName the office name of this users ext
	 */
	@Override
	public void setOfficeName(String officeName) {
		model.setOfficeName(officeName);
	}

	/**
	 * Sets the primary key of this users ext.
	 *
	 * @param primaryKey the primary key of this users ext
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the source of this users ext.
	 *
	 * @param source the source of this users ext
	 */
	@Override
	public void setSource(String source) {
		model.setSource(source);
	}

	/**
	 * Sets the user ID of this users ext.
	 *
	 * @param userId the user ID of this users ext
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this users ext.
	 *
	 * @param userName the user name of this users ext
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the users ext ID of this users ext.
	 *
	 * @param usersExtId the users ext ID of this users ext
	 */
	@Override
	public void setUsersExtId(long usersExtId) {
		model.setUsersExtId(usersExtId);
	}

	/**
	 * Sets the user uuid of this users ext.
	 *
	 * @param userUuid the user uuid of this users ext
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this users ext.
	 *
	 * @param uuid the uuid of this users ext
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected usersExtWrapper wrap(usersExt usersExt) {
		return new usersExtWrapper(usersExt);
	}

}