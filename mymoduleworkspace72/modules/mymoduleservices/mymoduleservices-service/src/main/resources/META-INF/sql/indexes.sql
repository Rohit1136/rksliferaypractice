create index IX_F9CD476F on rksModule_usersExt (designation[$COLUMN_LENGTH:75$]);
create index IX_DD5B9623 on rksModule_usersExt (officeId[$COLUMN_LENGTH:75$]);
create index IX_15F34A32 on rksModule_usersExt (userId);
create index IX_6EC70662 on rksModule_usersExt (userName[$COLUMN_LENGTH:75$]);
create index IX_78445DEC on rksModule_usersExt (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_5CE36F6E on rksModule_usersExt (uuid_[$COLUMN_LENGTH:75$], groupId);