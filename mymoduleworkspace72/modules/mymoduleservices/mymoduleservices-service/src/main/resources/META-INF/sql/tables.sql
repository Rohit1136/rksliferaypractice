create table rksModule_usersExt (
	uuid_ VARCHAR(75) null,
	usersExtId LONG not null primary key,
	officeId VARCHAR(75) null,
	officeName VARCHAR(75) null,
	designation VARCHAR(75) null,
	officeAddress VARCHAR(75) null,
	isActive BOOLEAN,
	source VARCHAR(75) null,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null
);