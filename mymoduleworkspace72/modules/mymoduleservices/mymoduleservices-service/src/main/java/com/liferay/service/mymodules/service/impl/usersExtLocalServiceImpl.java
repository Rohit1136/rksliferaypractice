/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.service.mymodules.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.service.mymodules.model.usersExt;
import com.liferay.service.mymodules.service.base.usersExtLocalServiceBaseImpl;

import java.util.List;

import org.osgi.service.component.annotations.Component;

/**
 * The implementation of the users ext local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>com.liferay.service.mymodules.service.usersExtLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author rohit
 * @see usersExtLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=com.liferay.service.mymodules.model.usersExt",
	service = AopService.class
)
public class usersExtLocalServiceImpl extends usersExtLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>com.liferay.service.mymodules.service.usersExtLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>com.liferay.service.mymodules.service.usersExtLocalServiceUtil</code>.
	 */
	
	public List<usersExt> findByUserExtId(long userExtId){
		return this.usersExtPersistence.findByUserExtId(userExtId);
	}
	
	public List<usersExt> findByOfficeId(String officeId){
		return this.usersExtPersistence.findByOfficeId(officeId);
	}
	
	public List<usersExt> findByUserId(long userId) {
		return this.usersExtPersistence.findByUserId(userId);
	}
	
	public List<usersExt> findByUserName(String userName) {
		return this.usersExtPersistence.findByUserName(userName);
	}
	
	public List<usersExt> findByDesignation(String designation){
		return this.usersExtPersistence.findByDesignation(designation);
	}
}