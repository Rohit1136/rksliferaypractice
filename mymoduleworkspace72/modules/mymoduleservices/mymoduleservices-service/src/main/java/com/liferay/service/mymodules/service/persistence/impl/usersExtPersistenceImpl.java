/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.service.mymodules.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.service.mymodules.exception.NoSuchusersExtException;
import com.liferay.service.mymodules.model.impl.usersExtImpl;
import com.liferay.service.mymodules.model.impl.usersExtModelImpl;
import com.liferay.service.mymodules.model.usersExt;
import com.liferay.service.mymodules.service.persistence.impl.constants.rksModulePersistenceConstants;
import com.liferay.service.mymodules.service.persistence.usersExtPersistence;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.annotation.versioning.ProviderType;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the users ext service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author rohit
 * @generated
 */
@Component(service = usersExtPersistence.class)
@ProviderType
public class usersExtPersistenceImpl
	extends BasePersistenceImpl<usersExt> implements usersExtPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>usersExtUtil</code> to access the users ext persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		usersExtImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the users exts where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching users exts
	 */
	@Override
	public List<usersExt> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the users exts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @return the range of matching users exts
	 */
	@Override
	public List<usersExt> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the users exts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByUuid(String, int, int, OrderByComparator)}
	 * @param uuid the uuid
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching users exts
	 */
	@Deprecated
	@Override
	public List<usersExt> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<usersExt> orderByComparator, boolean useFinderCache) {

		return findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the users exts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching users exts
	 */
	@Override
	public List<usersExt> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<usersExt> orderByComparator) {

		uuid = Objects.toString(uuid, "");

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindByUuid;
			finderArgs = new Object[] {uuid};
		}
		else {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<usersExt> list = (List<usersExt>)finderCache.getResult(
			finderPath, finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (usersExt usersExt : list) {
				if (!uuid.equals(usersExt.getUuid())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERSEXT_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(usersExtModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<usersExt>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<usersExt>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first users ext in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	@Override
	public usersExt findByUuid_First(
			String uuid, OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException {

		usersExt usersExt = fetchByUuid_First(uuid, orderByComparator);

		if (usersExt != null) {
			return usersExt;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append("}");

		throw new NoSuchusersExtException(msg.toString());
	}

	/**
	 * Returns the first users ext in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	@Override
	public usersExt fetchByUuid_First(
		String uuid, OrderByComparator<usersExt> orderByComparator) {

		List<usersExt> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last users ext in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	@Override
	public usersExt findByUuid_Last(
			String uuid, OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException {

		usersExt usersExt = fetchByUuid_Last(uuid, orderByComparator);

		if (usersExt != null) {
			return usersExt;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append("}");

		throw new NoSuchusersExtException(msg.toString());
	}

	/**
	 * Returns the last users ext in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	@Override
	public usersExt fetchByUuid_Last(
		String uuid, OrderByComparator<usersExt> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<usersExt> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the users exts before and after the current users ext in the ordered set where uuid = &#63;.
	 *
	 * @param usersExtId the primary key of the current users ext
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next users ext
	 * @throws NoSuchusersExtException if a users ext with the primary key could not be found
	 */
	@Override
	public usersExt[] findByUuid_PrevAndNext(
			long usersExtId, String uuid,
			OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException {

		uuid = Objects.toString(uuid, "");

		usersExt usersExt = findByPrimaryKey(usersExtId);

		Session session = null;

		try {
			session = openSession();

			usersExt[] array = new usersExtImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, usersExt, uuid, orderByComparator, true);

			array[1] = usersExt;

			array[2] = getByUuid_PrevAndNext(
				session, usersExt, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected usersExt getByUuid_PrevAndNext(
		Session session, usersExt usersExt, String uuid,
		OrderByComparator<usersExt> orderByComparator, boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERSEXT_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(usersExtModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(usersExt)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<usersExt> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the users exts where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (usersExt usersExt :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(usersExt);
		}
	}

	/**
	 * Returns the number of users exts where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching users exts
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERSEXT_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"usersExt.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(usersExt.uuid IS NULL OR usersExt.uuid = '')";

	private FinderPath _finderPathFetchByUUID_G;
	private FinderPath _finderPathCountByUUID_G;

	/**
	 * Returns the users ext where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchusersExtException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	@Override
	public usersExt findByUUID_G(String uuid, long groupId)
		throws NoSuchusersExtException {

		usersExt usersExt = fetchByUUID_G(uuid, groupId);

		if (usersExt == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("uuid=");
			msg.append(uuid);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchusersExtException(msg.toString());
		}

		return usersExt;
	}

	/**
	 * Returns the users ext where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #fetchByUUID_G(String,long)}
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	@Deprecated
	@Override
	public usersExt fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the users ext where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	@Override
	public usersExt fetchByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		Object[] finderArgs = new Object[] {uuid, groupId};

		Object result = finderCache.getResult(
			_finderPathFetchByUUID_G, finderArgs, this);

		if (result instanceof usersExt) {
			usersExt usersExt = (usersExt)result;

			if (!Objects.equals(uuid, usersExt.getUuid()) ||
				(groupId != usersExt.getGroupId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_USERSEXT_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				List<usersExt> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(
						_finderPathFetchByUUID_G, finderArgs, list);
				}
				else {
					usersExt usersExt = list.get(0);

					result = usersExt;

					cacheResult(usersExt);
				}
			}
			catch (Exception e) {
				finderCache.removeResult(_finderPathFetchByUUID_G, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (usersExt)result;
		}
	}

	/**
	 * Removes the users ext where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the users ext that was removed
	 */
	@Override
	public usersExt removeByUUID_G(String uuid, long groupId)
		throws NoSuchusersExtException {

		usersExt usersExt = findByUUID_G(uuid, groupId);

		return remove(usersExt);
	}

	/**
	 * Returns the number of users exts where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching users exts
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUUID_G;

		Object[] finderArgs = new Object[] {uuid, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_USERSEXT_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_2 =
		"usersExt.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_G_UUID_3 =
		"(usersExt.uuid IS NULL OR usersExt.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 =
		"usersExt.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the users exts where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching users exts
	 */
	@Override
	public List<usersExt> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the users exts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @return the range of matching users exts
	 */
	@Override
	public List<usersExt> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the users exts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByUuid_C(String,long, int, int, OrderByComparator)}
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching users exts
	 */
	@Deprecated
	@Override
	public List<usersExt> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<usersExt> orderByComparator, boolean useFinderCache) {

		return findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the users exts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching users exts
	 */
	@Override
	public List<usersExt> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<usersExt> orderByComparator) {

		uuid = Objects.toString(uuid, "");

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindByUuid_C;
			finderArgs = new Object[] {uuid, companyId};
		}
		else {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<usersExt> list = (List<usersExt>)finderCache.getResult(
			finderPath, finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (usersExt usersExt : list) {
				if (!uuid.equals(usersExt.getUuid()) ||
					(companyId != usersExt.getCompanyId())) {

					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_USERSEXT_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(usersExtModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<usersExt>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<usersExt>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first users ext in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	@Override
	public usersExt findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException {

		usersExt usersExt = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (usersExt != null) {
			return usersExt;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append("}");

		throw new NoSuchusersExtException(msg.toString());
	}

	/**
	 * Returns the first users ext in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	@Override
	public usersExt fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<usersExt> orderByComparator) {

		List<usersExt> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last users ext in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	@Override
	public usersExt findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException {

		usersExt usersExt = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (usersExt != null) {
			return usersExt;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append("}");

		throw new NoSuchusersExtException(msg.toString());
	}

	/**
	 * Returns the last users ext in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	@Override
	public usersExt fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<usersExt> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<usersExt> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the users exts before and after the current users ext in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param usersExtId the primary key of the current users ext
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next users ext
	 * @throws NoSuchusersExtException if a users ext with the primary key could not be found
	 */
	@Override
	public usersExt[] findByUuid_C_PrevAndNext(
			long usersExtId, String uuid, long companyId,
			OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException {

		uuid = Objects.toString(uuid, "");

		usersExt usersExt = findByPrimaryKey(usersExtId);

		Session session = null;

		try {
			session = openSession();

			usersExt[] array = new usersExtImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, usersExt, uuid, companyId, orderByComparator, true);

			array[1] = usersExt;

			array[2] = getByUuid_C_PrevAndNext(
				session, usersExt, uuid, companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected usersExt getByUuid_C_PrevAndNext(
		Session session, usersExt usersExt, String uuid, long companyId,
		OrderByComparator<usersExt> orderByComparator, boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_USERSEXT_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(usersExtModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(usersExt)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<usersExt> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the users exts where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (usersExt usersExt :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(usersExt);
		}
	}

	/**
	 * Returns the number of users exts where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching users exts
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_USERSEXT_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"usersExt.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(usersExt.uuid IS NULL OR usersExt.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"usersExt.companyId = ?";

	private FinderPath _finderPathWithPaginationFindByUserExtId;
	private FinderPath _finderPathWithoutPaginationFindByUserExtId;
	private FinderPath _finderPathCountByUserExtId;

	/**
	 * Returns all the users exts where usersExtId = &#63;.
	 *
	 * @param usersExtId the users ext ID
	 * @return the matching users exts
	 */
	@Override
	public List<usersExt> findByUserExtId(long usersExtId) {
		return findByUserExtId(
			usersExtId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the users exts where usersExtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param usersExtId the users ext ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @return the range of matching users exts
	 */
	@Override
	public List<usersExt> findByUserExtId(long usersExtId, int start, int end) {
		return findByUserExtId(usersExtId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the users exts where usersExtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByUserExtId(long, int, int, OrderByComparator)}
	 * @param usersExtId the users ext ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching users exts
	 */
	@Deprecated
	@Override
	public List<usersExt> findByUserExtId(
		long usersExtId, int start, int end,
		OrderByComparator<usersExt> orderByComparator, boolean useFinderCache) {

		return findByUserExtId(usersExtId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the users exts where usersExtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param usersExtId the users ext ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching users exts
	 */
	@Override
	public List<usersExt> findByUserExtId(
		long usersExtId, int start, int end,
		OrderByComparator<usersExt> orderByComparator) {

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindByUserExtId;
			finderArgs = new Object[] {usersExtId};
		}
		else {
			finderPath = _finderPathWithPaginationFindByUserExtId;
			finderArgs = new Object[] {
				usersExtId, start, end, orderByComparator
			};
		}

		List<usersExt> list = (List<usersExt>)finderCache.getResult(
			finderPath, finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (usersExt usersExt : list) {
				if ((usersExtId != usersExt.getUsersExtId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERSEXT_WHERE);

			query.append(_FINDER_COLUMN_USEREXTID_USERSEXTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(usersExtModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(usersExtId);

				if (!pagination) {
					list = (List<usersExt>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<usersExt>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first users ext in the ordered set where usersExtId = &#63;.
	 *
	 * @param usersExtId the users ext ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	@Override
	public usersExt findByUserExtId_First(
			long usersExtId, OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException {

		usersExt usersExt = fetchByUserExtId_First(
			usersExtId, orderByComparator);

		if (usersExt != null) {
			return usersExt;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("usersExtId=");
		msg.append(usersExtId);

		msg.append("}");

		throw new NoSuchusersExtException(msg.toString());
	}

	/**
	 * Returns the first users ext in the ordered set where usersExtId = &#63;.
	 *
	 * @param usersExtId the users ext ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	@Override
	public usersExt fetchByUserExtId_First(
		long usersExtId, OrderByComparator<usersExt> orderByComparator) {

		List<usersExt> list = findByUserExtId(
			usersExtId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last users ext in the ordered set where usersExtId = &#63;.
	 *
	 * @param usersExtId the users ext ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	@Override
	public usersExt findByUserExtId_Last(
			long usersExtId, OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException {

		usersExt usersExt = fetchByUserExtId_Last(
			usersExtId, orderByComparator);

		if (usersExt != null) {
			return usersExt;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("usersExtId=");
		msg.append(usersExtId);

		msg.append("}");

		throw new NoSuchusersExtException(msg.toString());
	}

	/**
	 * Returns the last users ext in the ordered set where usersExtId = &#63;.
	 *
	 * @param usersExtId the users ext ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	@Override
	public usersExt fetchByUserExtId_Last(
		long usersExtId, OrderByComparator<usersExt> orderByComparator) {

		int count = countByUserExtId(usersExtId);

		if (count == 0) {
			return null;
		}

		List<usersExt> list = findByUserExtId(
			usersExtId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Removes all the users exts where usersExtId = &#63; from the database.
	 *
	 * @param usersExtId the users ext ID
	 */
	@Override
	public void removeByUserExtId(long usersExtId) {
		for (usersExt usersExt :
				findByUserExtId(
					usersExtId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(usersExt);
		}
	}

	/**
	 * Returns the number of users exts where usersExtId = &#63;.
	 *
	 * @param usersExtId the users ext ID
	 * @return the number of matching users exts
	 */
	@Override
	public int countByUserExtId(long usersExtId) {
		FinderPath finderPath = _finderPathCountByUserExtId;

		Object[] finderArgs = new Object[] {usersExtId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERSEXT_WHERE);

			query.append(_FINDER_COLUMN_USEREXTID_USERSEXTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(usersExtId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USEREXTID_USERSEXTID_2 =
		"usersExt.usersExtId = ?";

	private FinderPath _finderPathWithPaginationFindByOfficeId;
	private FinderPath _finderPathWithoutPaginationFindByOfficeId;
	private FinderPath _finderPathCountByOfficeId;

	/**
	 * Returns all the users exts where officeId = &#63;.
	 *
	 * @param officeId the office ID
	 * @return the matching users exts
	 */
	@Override
	public List<usersExt> findByOfficeId(String officeId) {
		return findByOfficeId(
			officeId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the users exts where officeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param officeId the office ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @return the range of matching users exts
	 */
	@Override
	public List<usersExt> findByOfficeId(String officeId, int start, int end) {
		return findByOfficeId(officeId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the users exts where officeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByOfficeId(String, int, int, OrderByComparator)}
	 * @param officeId the office ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching users exts
	 */
	@Deprecated
	@Override
	public List<usersExt> findByOfficeId(
		String officeId, int start, int end,
		OrderByComparator<usersExt> orderByComparator, boolean useFinderCache) {

		return findByOfficeId(officeId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the users exts where officeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param officeId the office ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching users exts
	 */
	@Override
	public List<usersExt> findByOfficeId(
		String officeId, int start, int end,
		OrderByComparator<usersExt> orderByComparator) {

		officeId = Objects.toString(officeId, "");

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindByOfficeId;
			finderArgs = new Object[] {officeId};
		}
		else {
			finderPath = _finderPathWithPaginationFindByOfficeId;
			finderArgs = new Object[] {officeId, start, end, orderByComparator};
		}

		List<usersExt> list = (List<usersExt>)finderCache.getResult(
			finderPath, finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (usersExt usersExt : list) {
				if (!officeId.equals(usersExt.getOfficeId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERSEXT_WHERE);

			boolean bindOfficeId = false;

			if (officeId.isEmpty()) {
				query.append(_FINDER_COLUMN_OFFICEID_OFFICEID_3);
			}
			else {
				bindOfficeId = true;

				query.append(_FINDER_COLUMN_OFFICEID_OFFICEID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(usersExtModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindOfficeId) {
					qPos.add(officeId);
				}

				if (!pagination) {
					list = (List<usersExt>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<usersExt>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first users ext in the ordered set where officeId = &#63;.
	 *
	 * @param officeId the office ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	@Override
	public usersExt findByOfficeId_First(
			String officeId, OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException {

		usersExt usersExt = fetchByOfficeId_First(officeId, orderByComparator);

		if (usersExt != null) {
			return usersExt;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("officeId=");
		msg.append(officeId);

		msg.append("}");

		throw new NoSuchusersExtException(msg.toString());
	}

	/**
	 * Returns the first users ext in the ordered set where officeId = &#63;.
	 *
	 * @param officeId the office ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	@Override
	public usersExt fetchByOfficeId_First(
		String officeId, OrderByComparator<usersExt> orderByComparator) {

		List<usersExt> list = findByOfficeId(officeId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last users ext in the ordered set where officeId = &#63;.
	 *
	 * @param officeId the office ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	@Override
	public usersExt findByOfficeId_Last(
			String officeId, OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException {

		usersExt usersExt = fetchByOfficeId_Last(officeId, orderByComparator);

		if (usersExt != null) {
			return usersExt;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("officeId=");
		msg.append(officeId);

		msg.append("}");

		throw new NoSuchusersExtException(msg.toString());
	}

	/**
	 * Returns the last users ext in the ordered set where officeId = &#63;.
	 *
	 * @param officeId the office ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	@Override
	public usersExt fetchByOfficeId_Last(
		String officeId, OrderByComparator<usersExt> orderByComparator) {

		int count = countByOfficeId(officeId);

		if (count == 0) {
			return null;
		}

		List<usersExt> list = findByOfficeId(
			officeId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the users exts before and after the current users ext in the ordered set where officeId = &#63;.
	 *
	 * @param usersExtId the primary key of the current users ext
	 * @param officeId the office ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next users ext
	 * @throws NoSuchusersExtException if a users ext with the primary key could not be found
	 */
	@Override
	public usersExt[] findByOfficeId_PrevAndNext(
			long usersExtId, String officeId,
			OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException {

		officeId = Objects.toString(officeId, "");

		usersExt usersExt = findByPrimaryKey(usersExtId);

		Session session = null;

		try {
			session = openSession();

			usersExt[] array = new usersExtImpl[3];

			array[0] = getByOfficeId_PrevAndNext(
				session, usersExt, officeId, orderByComparator, true);

			array[1] = usersExt;

			array[2] = getByOfficeId_PrevAndNext(
				session, usersExt, officeId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected usersExt getByOfficeId_PrevAndNext(
		Session session, usersExt usersExt, String officeId,
		OrderByComparator<usersExt> orderByComparator, boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERSEXT_WHERE);

		boolean bindOfficeId = false;

		if (officeId.isEmpty()) {
			query.append(_FINDER_COLUMN_OFFICEID_OFFICEID_3);
		}
		else {
			bindOfficeId = true;

			query.append(_FINDER_COLUMN_OFFICEID_OFFICEID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(usersExtModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindOfficeId) {
			qPos.add(officeId);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(usersExt)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<usersExt> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the users exts where officeId = &#63; from the database.
	 *
	 * @param officeId the office ID
	 */
	@Override
	public void removeByOfficeId(String officeId) {
		for (usersExt usersExt :
				findByOfficeId(
					officeId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(usersExt);
		}
	}

	/**
	 * Returns the number of users exts where officeId = &#63;.
	 *
	 * @param officeId the office ID
	 * @return the number of matching users exts
	 */
	@Override
	public int countByOfficeId(String officeId) {
		officeId = Objects.toString(officeId, "");

		FinderPath finderPath = _finderPathCountByOfficeId;

		Object[] finderArgs = new Object[] {officeId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERSEXT_WHERE);

			boolean bindOfficeId = false;

			if (officeId.isEmpty()) {
				query.append(_FINDER_COLUMN_OFFICEID_OFFICEID_3);
			}
			else {
				bindOfficeId = true;

				query.append(_FINDER_COLUMN_OFFICEID_OFFICEID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindOfficeId) {
					qPos.add(officeId);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_OFFICEID_OFFICEID_2 =
		"usersExt.officeId = ?";

	private static final String _FINDER_COLUMN_OFFICEID_OFFICEID_3 =
		"(usersExt.officeId IS NULL OR usersExt.officeId = '')";

	private FinderPath _finderPathWithPaginationFindByUserId;
	private FinderPath _finderPathWithoutPaginationFindByUserId;
	private FinderPath _finderPathCountByUserId;

	/**
	 * Returns all the users exts where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching users exts
	 */
	@Override
	public List<usersExt> findByUserId(long userId) {
		return findByUserId(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the users exts where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @return the range of matching users exts
	 */
	@Override
	public List<usersExt> findByUserId(long userId, int start, int end) {
		return findByUserId(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the users exts where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByUserId(long, int, int, OrderByComparator)}
	 * @param userId the user ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching users exts
	 */
	@Deprecated
	@Override
	public List<usersExt> findByUserId(
		long userId, int start, int end,
		OrderByComparator<usersExt> orderByComparator, boolean useFinderCache) {

		return findByUserId(userId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the users exts where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching users exts
	 */
	@Override
	public List<usersExt> findByUserId(
		long userId, int start, int end,
		OrderByComparator<usersExt> orderByComparator) {

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindByUserId;
			finderArgs = new Object[] {userId};
		}
		else {
			finderPath = _finderPathWithPaginationFindByUserId;
			finderArgs = new Object[] {userId, start, end, orderByComparator};
		}

		List<usersExt> list = (List<usersExt>)finderCache.getResult(
			finderPath, finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (usersExt usersExt : list) {
				if ((userId != usersExt.getUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERSEXT_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(usersExtModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				if (!pagination) {
					list = (List<usersExt>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<usersExt>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first users ext in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	@Override
	public usersExt findByUserId_First(
			long userId, OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException {

		usersExt usersExt = fetchByUserId_First(userId, orderByComparator);

		if (usersExt != null) {
			return usersExt;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append("}");

		throw new NoSuchusersExtException(msg.toString());
	}

	/**
	 * Returns the first users ext in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	@Override
	public usersExt fetchByUserId_First(
		long userId, OrderByComparator<usersExt> orderByComparator) {

		List<usersExt> list = findByUserId(userId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last users ext in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	@Override
	public usersExt findByUserId_Last(
			long userId, OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException {

		usersExt usersExt = fetchByUserId_Last(userId, orderByComparator);

		if (usersExt != null) {
			return usersExt;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append("}");

		throw new NoSuchusersExtException(msg.toString());
	}

	/**
	 * Returns the last users ext in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	@Override
	public usersExt fetchByUserId_Last(
		long userId, OrderByComparator<usersExt> orderByComparator) {

		int count = countByUserId(userId);

		if (count == 0) {
			return null;
		}

		List<usersExt> list = findByUserId(
			userId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the users exts before and after the current users ext in the ordered set where userId = &#63;.
	 *
	 * @param usersExtId the primary key of the current users ext
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next users ext
	 * @throws NoSuchusersExtException if a users ext with the primary key could not be found
	 */
	@Override
	public usersExt[] findByUserId_PrevAndNext(
			long usersExtId, long userId,
			OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException {

		usersExt usersExt = findByPrimaryKey(usersExtId);

		Session session = null;

		try {
			session = openSession();

			usersExt[] array = new usersExtImpl[3];

			array[0] = getByUserId_PrevAndNext(
				session, usersExt, userId, orderByComparator, true);

			array[1] = usersExt;

			array[2] = getByUserId_PrevAndNext(
				session, usersExt, userId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected usersExt getByUserId_PrevAndNext(
		Session session, usersExt usersExt, long userId,
		OrderByComparator<usersExt> orderByComparator, boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERSEXT_WHERE);

		query.append(_FINDER_COLUMN_USERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(usersExtModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(usersExt)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<usersExt> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the users exts where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 */
	@Override
	public void removeByUserId(long userId) {
		for (usersExt usersExt :
				findByUserId(
					userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(usersExt);
		}
	}

	/**
	 * Returns the number of users exts where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching users exts
	 */
	@Override
	public int countByUserId(long userId) {
		FinderPath finderPath = _finderPathCountByUserId;

		Object[] finderArgs = new Object[] {userId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERSEXT_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERID_USERID_2 =
		"usersExt.userId = ?";

	private FinderPath _finderPathWithPaginationFindByUserName;
	private FinderPath _finderPathWithoutPaginationFindByUserName;
	private FinderPath _finderPathCountByUserName;

	/**
	 * Returns all the users exts where userName = &#63;.
	 *
	 * @param userName the user name
	 * @return the matching users exts
	 */
	@Override
	public List<usersExt> findByUserName(String userName) {
		return findByUserName(
			userName, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the users exts where userName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userName the user name
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @return the range of matching users exts
	 */
	@Override
	public List<usersExt> findByUserName(String userName, int start, int end) {
		return findByUserName(userName, start, end, null);
	}

	/**
	 * Returns an ordered range of all the users exts where userName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByUserName(String, int, int, OrderByComparator)}
	 * @param userName the user name
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching users exts
	 */
	@Deprecated
	@Override
	public List<usersExt> findByUserName(
		String userName, int start, int end,
		OrderByComparator<usersExt> orderByComparator, boolean useFinderCache) {

		return findByUserName(userName, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the users exts where userName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userName the user name
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching users exts
	 */
	@Override
	public List<usersExt> findByUserName(
		String userName, int start, int end,
		OrderByComparator<usersExt> orderByComparator) {

		userName = Objects.toString(userName, "");

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindByUserName;
			finderArgs = new Object[] {userName};
		}
		else {
			finderPath = _finderPathWithPaginationFindByUserName;
			finderArgs = new Object[] {userName, start, end, orderByComparator};
		}

		List<usersExt> list = (List<usersExt>)finderCache.getResult(
			finderPath, finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (usersExt usersExt : list) {
				if (!userName.equals(usersExt.getUserName())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERSEXT_WHERE);

			boolean bindUserName = false;

			if (userName.isEmpty()) {
				query.append(_FINDER_COLUMN_USERNAME_USERNAME_3);
			}
			else {
				bindUserName = true;

				query.append(_FINDER_COLUMN_USERNAME_USERNAME_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(usersExtModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUserName) {
					qPos.add(userName);
				}

				if (!pagination) {
					list = (List<usersExt>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<usersExt>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first users ext in the ordered set where userName = &#63;.
	 *
	 * @param userName the user name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	@Override
	public usersExt findByUserName_First(
			String userName, OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException {

		usersExt usersExt = fetchByUserName_First(userName, orderByComparator);

		if (usersExt != null) {
			return usersExt;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userName=");
		msg.append(userName);

		msg.append("}");

		throw new NoSuchusersExtException(msg.toString());
	}

	/**
	 * Returns the first users ext in the ordered set where userName = &#63;.
	 *
	 * @param userName the user name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	@Override
	public usersExt fetchByUserName_First(
		String userName, OrderByComparator<usersExt> orderByComparator) {

		List<usersExt> list = findByUserName(userName, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last users ext in the ordered set where userName = &#63;.
	 *
	 * @param userName the user name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	@Override
	public usersExt findByUserName_Last(
			String userName, OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException {

		usersExt usersExt = fetchByUserName_Last(userName, orderByComparator);

		if (usersExt != null) {
			return usersExt;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userName=");
		msg.append(userName);

		msg.append("}");

		throw new NoSuchusersExtException(msg.toString());
	}

	/**
	 * Returns the last users ext in the ordered set where userName = &#63;.
	 *
	 * @param userName the user name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	@Override
	public usersExt fetchByUserName_Last(
		String userName, OrderByComparator<usersExt> orderByComparator) {

		int count = countByUserName(userName);

		if (count == 0) {
			return null;
		}

		List<usersExt> list = findByUserName(
			userName, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the users exts before and after the current users ext in the ordered set where userName = &#63;.
	 *
	 * @param usersExtId the primary key of the current users ext
	 * @param userName the user name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next users ext
	 * @throws NoSuchusersExtException if a users ext with the primary key could not be found
	 */
	@Override
	public usersExt[] findByUserName_PrevAndNext(
			long usersExtId, String userName,
			OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException {

		userName = Objects.toString(userName, "");

		usersExt usersExt = findByPrimaryKey(usersExtId);

		Session session = null;

		try {
			session = openSession();

			usersExt[] array = new usersExtImpl[3];

			array[0] = getByUserName_PrevAndNext(
				session, usersExt, userName, orderByComparator, true);

			array[1] = usersExt;

			array[2] = getByUserName_PrevAndNext(
				session, usersExt, userName, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected usersExt getByUserName_PrevAndNext(
		Session session, usersExt usersExt, String userName,
		OrderByComparator<usersExt> orderByComparator, boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERSEXT_WHERE);

		boolean bindUserName = false;

		if (userName.isEmpty()) {
			query.append(_FINDER_COLUMN_USERNAME_USERNAME_3);
		}
		else {
			bindUserName = true;

			query.append(_FINDER_COLUMN_USERNAME_USERNAME_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(usersExtModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUserName) {
			qPos.add(userName);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(usersExt)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<usersExt> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the users exts where userName = &#63; from the database.
	 *
	 * @param userName the user name
	 */
	@Override
	public void removeByUserName(String userName) {
		for (usersExt usersExt :
				findByUserName(
					userName, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(usersExt);
		}
	}

	/**
	 * Returns the number of users exts where userName = &#63;.
	 *
	 * @param userName the user name
	 * @return the number of matching users exts
	 */
	@Override
	public int countByUserName(String userName) {
		userName = Objects.toString(userName, "");

		FinderPath finderPath = _finderPathCountByUserName;

		Object[] finderArgs = new Object[] {userName};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERSEXT_WHERE);

			boolean bindUserName = false;

			if (userName.isEmpty()) {
				query.append(_FINDER_COLUMN_USERNAME_USERNAME_3);
			}
			else {
				bindUserName = true;

				query.append(_FINDER_COLUMN_USERNAME_USERNAME_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUserName) {
					qPos.add(userName);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERNAME_USERNAME_2 =
		"usersExt.userName = ?";

	private static final String _FINDER_COLUMN_USERNAME_USERNAME_3 =
		"(usersExt.userName IS NULL OR usersExt.userName = '')";

	private FinderPath _finderPathWithPaginationFindByDesignation;
	private FinderPath _finderPathWithoutPaginationFindByDesignation;
	private FinderPath _finderPathCountByDesignation;

	/**
	 * Returns all the users exts where designation = &#63;.
	 *
	 * @param designation the designation
	 * @return the matching users exts
	 */
	@Override
	public List<usersExt> findByDesignation(String designation) {
		return findByDesignation(
			designation, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the users exts where designation = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param designation the designation
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @return the range of matching users exts
	 */
	@Override
	public List<usersExt> findByDesignation(
		String designation, int start, int end) {

		return findByDesignation(designation, start, end, null);
	}

	/**
	 * Returns an ordered range of all the users exts where designation = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByDesignation(String, int, int, OrderByComparator)}
	 * @param designation the designation
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching users exts
	 */
	@Deprecated
	@Override
	public List<usersExt> findByDesignation(
		String designation, int start, int end,
		OrderByComparator<usersExt> orderByComparator, boolean useFinderCache) {

		return findByDesignation(designation, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the users exts where designation = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param designation the designation
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching users exts
	 */
	@Override
	public List<usersExt> findByDesignation(
		String designation, int start, int end,
		OrderByComparator<usersExt> orderByComparator) {

		designation = Objects.toString(designation, "");

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindByDesignation;
			finderArgs = new Object[] {designation};
		}
		else {
			finderPath = _finderPathWithPaginationFindByDesignation;
			finderArgs = new Object[] {
				designation, start, end, orderByComparator
			};
		}

		List<usersExt> list = (List<usersExt>)finderCache.getResult(
			finderPath, finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (usersExt usersExt : list) {
				if (!designation.equals(usersExt.getDesignation())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERSEXT_WHERE);

			boolean bindDesignation = false;

			if (designation.isEmpty()) {
				query.append(_FINDER_COLUMN_DESIGNATION_DESIGNATION_3);
			}
			else {
				bindDesignation = true;

				query.append(_FINDER_COLUMN_DESIGNATION_DESIGNATION_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(usersExtModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindDesignation) {
					qPos.add(designation);
				}

				if (!pagination) {
					list = (List<usersExt>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<usersExt>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first users ext in the ordered set where designation = &#63;.
	 *
	 * @param designation the designation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	@Override
	public usersExt findByDesignation_First(
			String designation, OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException {

		usersExt usersExt = fetchByDesignation_First(
			designation, orderByComparator);

		if (usersExt != null) {
			return usersExt;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("designation=");
		msg.append(designation);

		msg.append("}");

		throw new NoSuchusersExtException(msg.toString());
	}

	/**
	 * Returns the first users ext in the ordered set where designation = &#63;.
	 *
	 * @param designation the designation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	@Override
	public usersExt fetchByDesignation_First(
		String designation, OrderByComparator<usersExt> orderByComparator) {

		List<usersExt> list = findByDesignation(
			designation, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last users ext in the ordered set where designation = &#63;.
	 *
	 * @param designation the designation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext
	 * @throws NoSuchusersExtException if a matching users ext could not be found
	 */
	@Override
	public usersExt findByDesignation_Last(
			String designation, OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException {

		usersExt usersExt = fetchByDesignation_Last(
			designation, orderByComparator);

		if (usersExt != null) {
			return usersExt;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("designation=");
		msg.append(designation);

		msg.append("}");

		throw new NoSuchusersExtException(msg.toString());
	}

	/**
	 * Returns the last users ext in the ordered set where designation = &#63;.
	 *
	 * @param designation the designation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users ext, or <code>null</code> if a matching users ext could not be found
	 */
	@Override
	public usersExt fetchByDesignation_Last(
		String designation, OrderByComparator<usersExt> orderByComparator) {

		int count = countByDesignation(designation);

		if (count == 0) {
			return null;
		}

		List<usersExt> list = findByDesignation(
			designation, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the users exts before and after the current users ext in the ordered set where designation = &#63;.
	 *
	 * @param usersExtId the primary key of the current users ext
	 * @param designation the designation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next users ext
	 * @throws NoSuchusersExtException if a users ext with the primary key could not be found
	 */
	@Override
	public usersExt[] findByDesignation_PrevAndNext(
			long usersExtId, String designation,
			OrderByComparator<usersExt> orderByComparator)
		throws NoSuchusersExtException {

		designation = Objects.toString(designation, "");

		usersExt usersExt = findByPrimaryKey(usersExtId);

		Session session = null;

		try {
			session = openSession();

			usersExt[] array = new usersExtImpl[3];

			array[0] = getByDesignation_PrevAndNext(
				session, usersExt, designation, orderByComparator, true);

			array[1] = usersExt;

			array[2] = getByDesignation_PrevAndNext(
				session, usersExt, designation, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected usersExt getByDesignation_PrevAndNext(
		Session session, usersExt usersExt, String designation,
		OrderByComparator<usersExt> orderByComparator, boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERSEXT_WHERE);

		boolean bindDesignation = false;

		if (designation.isEmpty()) {
			query.append(_FINDER_COLUMN_DESIGNATION_DESIGNATION_3);
		}
		else {
			bindDesignation = true;

			query.append(_FINDER_COLUMN_DESIGNATION_DESIGNATION_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(usersExtModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindDesignation) {
			qPos.add(designation);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(usersExt)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<usersExt> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the users exts where designation = &#63; from the database.
	 *
	 * @param designation the designation
	 */
	@Override
	public void removeByDesignation(String designation) {
		for (usersExt usersExt :
				findByDesignation(
					designation, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(usersExt);
		}
	}

	/**
	 * Returns the number of users exts where designation = &#63;.
	 *
	 * @param designation the designation
	 * @return the number of matching users exts
	 */
	@Override
	public int countByDesignation(String designation) {
		designation = Objects.toString(designation, "");

		FinderPath finderPath = _finderPathCountByDesignation;

		Object[] finderArgs = new Object[] {designation};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERSEXT_WHERE);

			boolean bindDesignation = false;

			if (designation.isEmpty()) {
				query.append(_FINDER_COLUMN_DESIGNATION_DESIGNATION_3);
			}
			else {
				bindDesignation = true;

				query.append(_FINDER_COLUMN_DESIGNATION_DESIGNATION_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindDesignation) {
					qPos.add(designation);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_DESIGNATION_DESIGNATION_2 =
		"usersExt.designation = ?";

	private static final String _FINDER_COLUMN_DESIGNATION_DESIGNATION_3 =
		"(usersExt.designation IS NULL OR usersExt.designation = '')";

	public usersExtPersistenceImpl() {
		setModelClass(usersExt.class);

		setModelImplClass(usersExtImpl.class);
		setModelPKClass(long.class);

		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");

		setDBColumnNames(dbColumnNames);
	}

	/**
	 * Caches the users ext in the entity cache if it is enabled.
	 *
	 * @param usersExt the users ext
	 */
	@Override
	public void cacheResult(usersExt usersExt) {
		entityCache.putResult(
			entityCacheEnabled, usersExtImpl.class, usersExt.getPrimaryKey(),
			usersExt);

		finderCache.putResult(
			_finderPathFetchByUUID_G,
			new Object[] {usersExt.getUuid(), usersExt.getGroupId()}, usersExt);

		usersExt.resetOriginalValues();
	}

	/**
	 * Caches the users exts in the entity cache if it is enabled.
	 *
	 * @param usersExts the users exts
	 */
	@Override
	public void cacheResult(List<usersExt> usersExts) {
		for (usersExt usersExt : usersExts) {
			if (entityCache.getResult(
					entityCacheEnabled, usersExtImpl.class,
					usersExt.getPrimaryKey()) == null) {

				cacheResult(usersExt);
			}
			else {
				usersExt.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all users exts.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(usersExtImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the users ext.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(usersExt usersExt) {
		entityCache.removeResult(
			entityCacheEnabled, usersExtImpl.class, usersExt.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((usersExtModelImpl)usersExt, true);
	}

	@Override
	public void clearCache(List<usersExt> usersExts) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (usersExt usersExt : usersExts) {
			entityCache.removeResult(
				entityCacheEnabled, usersExtImpl.class,
				usersExt.getPrimaryKey());

			clearUniqueFindersCache((usersExtModelImpl)usersExt, true);
		}
	}

	protected void cacheUniqueFindersCache(
		usersExtModelImpl usersExtModelImpl) {

		Object[] args = new Object[] {
			usersExtModelImpl.getUuid(), usersExtModelImpl.getGroupId()
		};

		finderCache.putResult(
			_finderPathCountByUUID_G, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByUUID_G, args, usersExtModelImpl, false);
	}

	protected void clearUniqueFindersCache(
		usersExtModelImpl usersExtModelImpl, boolean clearCurrent) {

		if (clearCurrent) {
			Object[] args = new Object[] {
				usersExtModelImpl.getUuid(), usersExtModelImpl.getGroupId()
			};

			finderCache.removeResult(_finderPathCountByUUID_G, args);
			finderCache.removeResult(_finderPathFetchByUUID_G, args);
		}

		if ((usersExtModelImpl.getColumnBitmask() &
			 _finderPathFetchByUUID_G.getColumnBitmask()) != 0) {

			Object[] args = new Object[] {
				usersExtModelImpl.getOriginalUuid(),
				usersExtModelImpl.getOriginalGroupId()
			};

			finderCache.removeResult(_finderPathCountByUUID_G, args);
			finderCache.removeResult(_finderPathFetchByUUID_G, args);
		}
	}

	/**
	 * Creates a new users ext with the primary key. Does not add the users ext to the database.
	 *
	 * @param usersExtId the primary key for the new users ext
	 * @return the new users ext
	 */
	@Override
	public usersExt create(long usersExtId) {
		usersExt usersExt = new usersExtImpl();

		usersExt.setNew(true);
		usersExt.setPrimaryKey(usersExtId);

		String uuid = PortalUUIDUtil.generate();

		usersExt.setUuid(uuid);

		usersExt.setCompanyId(CompanyThreadLocal.getCompanyId());

		return usersExt;
	}

	/**
	 * Removes the users ext with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param usersExtId the primary key of the users ext
	 * @return the users ext that was removed
	 * @throws NoSuchusersExtException if a users ext with the primary key could not be found
	 */
	@Override
	public usersExt remove(long usersExtId) throws NoSuchusersExtException {
		return remove((Serializable)usersExtId);
	}

	/**
	 * Removes the users ext with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the users ext
	 * @return the users ext that was removed
	 * @throws NoSuchusersExtException if a users ext with the primary key could not be found
	 */
	@Override
	public usersExt remove(Serializable primaryKey)
		throws NoSuchusersExtException {

		Session session = null;

		try {
			session = openSession();

			usersExt usersExt = (usersExt)session.get(
				usersExtImpl.class, primaryKey);

			if (usersExt == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchusersExtException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(usersExt);
		}
		catch (NoSuchusersExtException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected usersExt removeImpl(usersExt usersExt) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(usersExt)) {
				usersExt = (usersExt)session.get(
					usersExtImpl.class, usersExt.getPrimaryKeyObj());
			}

			if (usersExt != null) {
				session.delete(usersExt);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (usersExt != null) {
			clearCache(usersExt);
		}

		return usersExt;
	}

	@Override
	public usersExt updateImpl(usersExt usersExt) {
		boolean isNew = usersExt.isNew();

		if (!(usersExt instanceof usersExtModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(usersExt.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(usersExt);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in usersExt proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom usersExt implementation " +
					usersExt.getClass());
		}

		usersExtModelImpl usersExtModelImpl = (usersExtModelImpl)usersExt;

		if (Validator.isNull(usersExt.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			usersExt.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (usersExt.getCreateDate() == null)) {
			if (serviceContext == null) {
				usersExt.setCreateDate(now);
			}
			else {
				usersExt.setCreateDate(serviceContext.getCreateDate(now));
			}
		}

		if (!usersExtModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				usersExt.setModifiedDate(now);
			}
			else {
				usersExt.setModifiedDate(serviceContext.getModifiedDate(now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (usersExt.isNew()) {
				session.save(usersExt);

				usersExt.setNew(false);
			}
			else {
				usersExt = (usersExt)session.merge(usersExt);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!_columnBitmaskEnabled) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else if (isNew) {
			Object[] args = new Object[] {usersExtModelImpl.getUuid()};

			finderCache.removeResult(_finderPathCountByUuid, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByUuid, args);

			args = new Object[] {
				usersExtModelImpl.getUuid(), usersExtModelImpl.getCompanyId()
			};

			finderCache.removeResult(_finderPathCountByUuid_C, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByUuid_C, args);

			args = new Object[] {usersExtModelImpl.getUsersExtId()};

			finderCache.removeResult(_finderPathCountByUserExtId, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByUserExtId, args);

			args = new Object[] {usersExtModelImpl.getOfficeId()};

			finderCache.removeResult(_finderPathCountByOfficeId, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByOfficeId, args);

			args = new Object[] {usersExtModelImpl.getUserId()};

			finderCache.removeResult(_finderPathCountByUserId, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByUserId, args);

			args = new Object[] {usersExtModelImpl.getUserName()};

			finderCache.removeResult(_finderPathCountByUserName, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByUserName, args);

			args = new Object[] {usersExtModelImpl.getDesignation()};

			finderCache.removeResult(_finderPathCountByDesignation, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByDesignation, args);

			finderCache.removeResult(_finderPathCountAll, FINDER_ARGS_EMPTY);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindAll, FINDER_ARGS_EMPTY);
		}
		else {
			if ((usersExtModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByUuid.getColumnBitmask()) !=
					 0) {

				Object[] args = new Object[] {
					usersExtModelImpl.getOriginalUuid()
				};

				finderCache.removeResult(_finderPathCountByUuid, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid, args);

				args = new Object[] {usersExtModelImpl.getUuid()};

				finderCache.removeResult(_finderPathCountByUuid, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid, args);
			}

			if ((usersExtModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByUuid_C.getColumnBitmask()) !=
					 0) {

				Object[] args = new Object[] {
					usersExtModelImpl.getOriginalUuid(),
					usersExtModelImpl.getOriginalCompanyId()
				};

				finderCache.removeResult(_finderPathCountByUuid_C, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid_C, args);

				args = new Object[] {
					usersExtModelImpl.getUuid(),
					usersExtModelImpl.getCompanyId()
				};

				finderCache.removeResult(_finderPathCountByUuid_C, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid_C, args);
			}

			if ((usersExtModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByUserExtId.
					 getColumnBitmask()) != 0) {

				Object[] args = new Object[] {
					usersExtModelImpl.getOriginalUsersExtId()
				};

				finderCache.removeResult(_finderPathCountByUserExtId, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUserExtId, args);

				args = new Object[] {usersExtModelImpl.getUsersExtId()};

				finderCache.removeResult(_finderPathCountByUserExtId, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUserExtId, args);
			}

			if ((usersExtModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByOfficeId.
					 getColumnBitmask()) != 0) {

				Object[] args = new Object[] {
					usersExtModelImpl.getOriginalOfficeId()
				};

				finderCache.removeResult(_finderPathCountByOfficeId, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByOfficeId, args);

				args = new Object[] {usersExtModelImpl.getOfficeId()};

				finderCache.removeResult(_finderPathCountByOfficeId, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByOfficeId, args);
			}

			if ((usersExtModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByUserId.getColumnBitmask()) !=
					 0) {

				Object[] args = new Object[] {
					usersExtModelImpl.getOriginalUserId()
				};

				finderCache.removeResult(_finderPathCountByUserId, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUserId, args);

				args = new Object[] {usersExtModelImpl.getUserId()};

				finderCache.removeResult(_finderPathCountByUserId, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUserId, args);
			}

			if ((usersExtModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByUserName.
					 getColumnBitmask()) != 0) {

				Object[] args = new Object[] {
					usersExtModelImpl.getOriginalUserName()
				};

				finderCache.removeResult(_finderPathCountByUserName, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUserName, args);

				args = new Object[] {usersExtModelImpl.getUserName()};

				finderCache.removeResult(_finderPathCountByUserName, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUserName, args);
			}

			if ((usersExtModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByDesignation.
					 getColumnBitmask()) != 0) {

				Object[] args = new Object[] {
					usersExtModelImpl.getOriginalDesignation()
				};

				finderCache.removeResult(_finderPathCountByDesignation, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByDesignation, args);

				args = new Object[] {usersExtModelImpl.getDesignation()};

				finderCache.removeResult(_finderPathCountByDesignation, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByDesignation, args);
			}
		}

		entityCache.putResult(
			entityCacheEnabled, usersExtImpl.class, usersExt.getPrimaryKey(),
			usersExt, false);

		clearUniqueFindersCache(usersExtModelImpl, false);
		cacheUniqueFindersCache(usersExtModelImpl);

		usersExt.resetOriginalValues();

		return usersExt;
	}

	/**
	 * Returns the users ext with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the users ext
	 * @return the users ext
	 * @throws NoSuchusersExtException if a users ext with the primary key could not be found
	 */
	@Override
	public usersExt findByPrimaryKey(Serializable primaryKey)
		throws NoSuchusersExtException {

		usersExt usersExt = fetchByPrimaryKey(primaryKey);

		if (usersExt == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchusersExtException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return usersExt;
	}

	/**
	 * Returns the users ext with the primary key or throws a <code>NoSuchusersExtException</code> if it could not be found.
	 *
	 * @param usersExtId the primary key of the users ext
	 * @return the users ext
	 * @throws NoSuchusersExtException if a users ext with the primary key could not be found
	 */
	@Override
	public usersExt findByPrimaryKey(long usersExtId)
		throws NoSuchusersExtException {

		return findByPrimaryKey((Serializable)usersExtId);
	}

	/**
	 * Returns the users ext with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param usersExtId the primary key of the users ext
	 * @return the users ext, or <code>null</code> if a users ext with the primary key could not be found
	 */
	@Override
	public usersExt fetchByPrimaryKey(long usersExtId) {
		return fetchByPrimaryKey((Serializable)usersExtId);
	}

	/**
	 * Returns all the users exts.
	 *
	 * @return the users exts
	 */
	@Override
	public List<usersExt> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the users exts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @return the range of users exts
	 */
	@Override
	public List<usersExt> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the users exts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findAll(int, int, OrderByComparator)}
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of users exts
	 */
	@Deprecated
	@Override
	public List<usersExt> findAll(
		int start, int end, OrderByComparator<usersExt> orderByComparator,
		boolean useFinderCache) {

		return findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the users exts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>usersExtModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of users exts
	 * @param end the upper bound of the range of users exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of users exts
	 */
	@Override
	public List<usersExt> findAll(
		int start, int end, OrderByComparator<usersExt> orderByComparator) {

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindAll;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<usersExt> list = (List<usersExt>)finderCache.getResult(
			finderPath, finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_USERSEXT);

				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_USERSEXT;

				if (pagination) {
					sql = sql.concat(usersExtModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<usersExt>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<usersExt>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the users exts from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (usersExt usersExt : findAll()) {
			remove(usersExt);
		}
	}

	/**
	 * Returns the number of users exts.
	 *
	 * @return the number of users exts
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_USERSEXT);

				count = (Long)q.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				finderCache.removeResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "usersExtId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_USERSEXT;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return usersExtModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the users ext persistence.
	 */
	@Activate
	public void activate() {
		usersExtModelImpl.setEntityCacheEnabled(entityCacheEnabled);
		usersExtModelImpl.setFinderCacheEnabled(finderCacheEnabled);

		_finderPathWithPaginationFindAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, usersExtImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, usersExtImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll",
			new String[0]);

		_finderPathCountAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0]);

		_finderPathWithPaginationFindByUuid = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, usersExtImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByUuid = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, usersExtImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()},
			usersExtModelImpl.UUID_COLUMN_BITMASK |
			usersExtModelImpl.OFFICEID_COLUMN_BITMASK);

		_finderPathCountByUuid = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()});

		_finderPathFetchByUUID_G = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, usersExtImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			usersExtModelImpl.UUID_COLUMN_BITMASK |
			usersExtModelImpl.GROUPID_COLUMN_BITMASK);

		_finderPathCountByUUID_G = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()});

		_finderPathWithPaginationFindByUuid_C = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, usersExtImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByUuid_C = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, usersExtImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			usersExtModelImpl.UUID_COLUMN_BITMASK |
			usersExtModelImpl.COMPANYID_COLUMN_BITMASK |
			usersExtModelImpl.OFFICEID_COLUMN_BITMASK);

		_finderPathCountByUuid_C = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()});

		_finderPathWithPaginationFindByUserExtId = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, usersExtImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserExtId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByUserExtId = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, usersExtImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUserExtId",
			new String[] {Long.class.getName()},
			usersExtModelImpl.USERSEXTID_COLUMN_BITMASK |
			usersExtModelImpl.OFFICEID_COLUMN_BITMASK);

		_finderPathCountByUserExtId = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUserExtId",
			new String[] {Long.class.getName()});

		_finderPathWithPaginationFindByOfficeId = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, usersExtImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByOfficeId",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByOfficeId = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, usersExtImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByOfficeId",
			new String[] {String.class.getName()},
			usersExtModelImpl.OFFICEID_COLUMN_BITMASK);

		_finderPathCountByOfficeId = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByOfficeId",
			new String[] {String.class.getName()});

		_finderPathWithPaginationFindByUserId = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, usersExtImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByUserId = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, usersExtImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUserId",
			new String[] {Long.class.getName()},
			usersExtModelImpl.USERID_COLUMN_BITMASK |
			usersExtModelImpl.OFFICEID_COLUMN_BITMASK);

		_finderPathCountByUserId = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUserId",
			new String[] {Long.class.getName()});

		_finderPathWithPaginationFindByUserName = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, usersExtImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserName",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByUserName = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, usersExtImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUserName",
			new String[] {String.class.getName()},
			usersExtModelImpl.USERNAME_COLUMN_BITMASK |
			usersExtModelImpl.OFFICEID_COLUMN_BITMASK);

		_finderPathCountByUserName = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUserName",
			new String[] {String.class.getName()});

		_finderPathWithPaginationFindByDesignation = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, usersExtImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByDesignation",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByDesignation = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, usersExtImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByDesignation",
			new String[] {String.class.getName()},
			usersExtModelImpl.DESIGNATION_COLUMN_BITMASK |
			usersExtModelImpl.OFFICEID_COLUMN_BITMASK);

		_finderPathCountByDesignation = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByDesignation",
			new String[] {String.class.getName()});
	}

	@Deactivate
	public void deactivate() {
		entityCache.removeCache(usersExtImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	@Reference(
		target = rksModulePersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
		super.setConfiguration(configuration);

		_columnBitmaskEnabled = GetterUtil.getBoolean(
			configuration.get(
				"value.object.column.bitmask.enabled.com.liferay.service.mymodules.model.usersExt"),
			true);
	}

	@Override
	@Reference(
		target = rksModulePersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = rksModulePersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	private boolean _columnBitmaskEnabled;

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_USERSEXT =
		"SELECT usersExt FROM usersExt usersExt";

	private static final String _SQL_SELECT_USERSEXT_WHERE =
		"SELECT usersExt FROM usersExt usersExt WHERE ";

	private static final String _SQL_COUNT_USERSEXT =
		"SELECT COUNT(usersExt) FROM usersExt usersExt";

	private static final String _SQL_COUNT_USERSEXT_WHERE =
		"SELECT COUNT(usersExt) FROM usersExt usersExt WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "usersExt.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No usersExt exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No usersExt exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		usersExtPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid"});

	static {
		try {
			Class.forName(rksModulePersistenceConstants.class.getName());
		}
		catch (ClassNotFoundException cnfe) {
			throw new ExceptionInInitializerError(cnfe);
		}
	}

}