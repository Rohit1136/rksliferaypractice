/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.service.mymodules.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.service.mymodules.model.usersExt;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The cache model class for representing usersExt in entity cache.
 *
 * @author rohit
 * @generated
 */
@ProviderType
public class usersExtCacheModel
	implements CacheModel<usersExt>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof usersExtCacheModel)) {
			return false;
		}

		usersExtCacheModel usersExtCacheModel = (usersExtCacheModel)obj;

		if (usersExtId == usersExtCacheModel.usersExtId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, usersExtId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(29);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", usersExtId=");
		sb.append(usersExtId);
		sb.append(", officeId=");
		sb.append(officeId);
		sb.append(", officeName=");
		sb.append(officeName);
		sb.append(", designation=");
		sb.append(designation);
		sb.append(", officeAddress=");
		sb.append(officeAddress);
		sb.append(", isActive=");
		sb.append(isActive);
		sb.append(", source=");
		sb.append(source);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public usersExt toEntityModel() {
		usersExtImpl usersExtImpl = new usersExtImpl();

		if (uuid == null) {
			usersExtImpl.setUuid("");
		}
		else {
			usersExtImpl.setUuid(uuid);
		}

		usersExtImpl.setUsersExtId(usersExtId);

		if (officeId == null) {
			usersExtImpl.setOfficeId("");
		}
		else {
			usersExtImpl.setOfficeId(officeId);
		}

		if (officeName == null) {
			usersExtImpl.setOfficeName("");
		}
		else {
			usersExtImpl.setOfficeName(officeName);
		}

		if (designation == null) {
			usersExtImpl.setDesignation("");
		}
		else {
			usersExtImpl.setDesignation(designation);
		}

		if (officeAddress == null) {
			usersExtImpl.setOfficeAddress("");
		}
		else {
			usersExtImpl.setOfficeAddress(officeAddress);
		}

		usersExtImpl.setIsActive(isActive);

		if (source == null) {
			usersExtImpl.setSource("");
		}
		else {
			usersExtImpl.setSource(source);
		}

		usersExtImpl.setGroupId(groupId);
		usersExtImpl.setCompanyId(companyId);
		usersExtImpl.setUserId(userId);

		if (userName == null) {
			usersExtImpl.setUserName("");
		}
		else {
			usersExtImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			usersExtImpl.setCreateDate(null);
		}
		else {
			usersExtImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			usersExtImpl.setModifiedDate(null);
		}
		else {
			usersExtImpl.setModifiedDate(new Date(modifiedDate));
		}

		usersExtImpl.resetOriginalValues();

		return usersExtImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		usersExtId = objectInput.readLong();
		officeId = objectInput.readUTF();
		officeName = objectInput.readUTF();
		designation = objectInput.readUTF();
		officeAddress = objectInput.readUTF();

		isActive = objectInput.readBoolean();
		source = objectInput.readUTF();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(usersExtId);

		if (officeId == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(officeId);
		}

		if (officeName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(officeName);
		}

		if (designation == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(designation);
		}

		if (officeAddress == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(officeAddress);
		}

		objectOutput.writeBoolean(isActive);

		if (source == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(source);
		}

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);
	}

	public String uuid;
	public long usersExtId;
	public String officeId;
	public String officeName;
	public String designation;
	public String officeAddress;
	public boolean isActive;
	public String source;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;

}